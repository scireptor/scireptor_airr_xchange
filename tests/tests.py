import pandas as pd
import scirexport.sr_tools as sr
from unittest.mock import Mock, patch
import os
import sys
import filecmp
import shutil
import builtins
from unittest import TestCase
from _pytest.monkeypatch import MonkeyPatch
import MySQLdb.cursors
import types
from typing import Union


# hacky import to override std in
def import_args(fn: str) -> types.ModuleType:
    """
    function to override the parameters expected from the main() scripts

    :param fn: name of the module to override parameters
    :return: imported module
    """
    if fn == "export":
        sys.argv = ["airr_export", "-d test_main", "-o .", "-s test"]
        from scirexport import airr_export

        return airr_export
    elif fn == "import":
        sys.argv = ["airr_import", "-n test_main_airr", "-i .", "-g test"]
        from scirexport import airr_import

        return airr_import


# import main modules for testing
aex = import_args("export")
aim = import_args("import")

config_dic = {
    "user": "root",
    "password": "",
    "database": "test_main",
    "host": "mariadb",  # gitignore
    "port": 3306,
}


def equal_dirs(dir_a: str, dir_b: str) -> bool:

    """
    Check that structure and files are the same for directories a and b

    :param dir_a: The path to the first directory
    :param dir_b: The path to the second directory
    :return: True if the files in the directory are identical
    """
    comp = filecmp.dircmp(dir_a, dir_b)
    common = sorted(comp.common)
    left = sorted(comp.left_list)
    right = sorted(comp.right_list)
    if left != common or right != common:
        return False
    if len(comp.diff_files):
        return False
    for subdir in comp.common_dirs:
        left_subdir = os.path.join(dir_a, subdir)
        right_subdir = os.path.join(dir_b, subdir)
        return equal_dirs(left_subdir, right_subdir)
    return True


def get_test_directory(test_db: str) -> str:
    """

    :param test_db: database name to retrieve
    :return: str path to a database file
    """
    # relative package paths
    this_dir, _ = os.path.split(__file__)

    if test_db == "test_meta":
        file_db = "test_meta_db-202107221621.sql"
    elif test_db == "test_onto":
        file_db = "test_onto_db-202107230942.sql"
    elif test_db == "test_receptors":
        file_db = "test_receptors_db-202108031022.sql"
    else:
        file_db = "test_main_db-202107261257.sql"
    # the test main db
    return os.path.join(this_dir, "data", "database", file_db)


def execute_one_db(config: Union[dict, str], db_name: str) -> None:
    """

    :param config: dictionary with the connection parameters
    :param db_name: name of the database to import.
    :return: None, creates a database
    """
    # connect to the created database
    cnx = sr.connect(config, db_name, ssh_group="test")

    cursor = cnx.cursor()

    db_string = sr.mysql_as_string(get_test_directory(db_name))

    try:
        cursor.execute(db_string)
        cnx.commit()
    except MySQLdb.Error as err:
        print(err)
    else:
        print("OK")

    cursor.close()
    cnx.close()


class TestUtils(TestCase):

    this_dir, _ = os.path.split(__file__)
    test_files = os.path.join(this_dir, "data", "test_data")
    test_dir = os.path.join(this_dir, "tmp_path")

    @classmethod
    def setUpClass(cls):
        cnx = sr.connect(config_dic, ssh_group="test")

        cls.monkeypatch = MonkeyPatch()
        cls.monkeypatch.setitem(aex.DB_ARGS, "config_db", config_dic)
        cls.monkeypatch.setitem(aex.DB_ARGS, "database", "test_main")
        cls.monkeypatch.setitem(aex.DB_ARGS, "output_path", cls.test_dir)

        cls.monkeypatch.setitem(aim.DB_ARGS, "config_db", config_dic)
        cls.monkeypatch.setitem(aim.DB_ARGS, "project_name", "test_main_airr")
        cls.monkeypatch.setitem(aim.DB_ARGS, "ssh_group", "test")
        cls.monkeypatch.setitem(aim.DB_ARGS, "input_path", cls.test_files)

        # delete tmp folder if already exists
        if os.path.exists(cls.test_dir):
            try:
                shutil.rmtree(cls.test_dir)
            except OSError as e:
                print("Error: %s : %s" % (cls.test_dir, e.strerror))

        os.mkdir(cls.test_dir)

        # drop database if it already exists
        for db in ["test_main", "test_onto", "test_meta", "test_receptors"]:

            cursor = cnx.cursor()
            try:
                cursor.execute("DROP DATABASE IF EXISTS {}".format(db))
                cursor.execute("DROP DATABASE IF EXISTS test_main_airr")
                cursor.close()
                print(f"{db} DB dropped")
            except MySQLdb.Error as err:
                print(f"{db} {err}")

            cursor = cnx.cursor()
            try:
                cursor.execute(f"CREATE DATABASE {db} DEFAULT CHARACTER SET 'latin1'")
                print(f"{db} DB created")

            except MySQLdb.Error as err:
                print(f"Failed creating database: {db} {err}")
                exit(1)

            cursor.close()
        cnx.close()
        # connect to the created database

        for db in ["test_main", "test_onto", "test_meta", "test_receptors"]:
            execute_one_db(config_dic, db)

    def test_export_end(self):
        """End to end test for the exporter module.
        Exports the defined set of files from the test_db and asserts equality against the
        sample files on `test_data`"""

        assert aex.main() is True
        assert (
            equal_dirs(os.path.join(self.test_dir, "test_main"), self.test_files)
            is True
        )

    def test_import_end(self):
        """End to end test for the importer module. Import the data from the sample files on
        `test_data` to the mocked database as `test_main_airr` db"""

        assert aim.main() is True

    # pd.testing.assert_frame_equal(rearrangements_frame, ontologies_df)  # todo test

    def test_filter_cells(self):
        """mock the standard input for testing input_cell_types()"""
        cnx = sr.connect(config_dic, "test_main", ssh_group="test")
        self.monkeypatch.setitem(aex.DB_ARGS, "filter_cells", True)
        input_cell_types = aex.input_cell_types
        with patch.object(builtins, "input", lambda _: "0"):
            #     aex.input_cell_types() == ""
            print(input_cell_types("test_main", cnx))
            assert list(input_cell_types("test_main", cnx)[0]) == ["GC"]
        cnx.close()

    def test_db_read(self):

        """toy test"""

        cnx = sr.connect(config_dic, "test_main", ssh_group="test")
        cursor = cnx.cursor()

        self.assertDictEqual(
            pd.read_sql(
                f"SELECT reads.consensus_id as seq_id ,COUNT(DISTINCT(seq_id)) as duplicate_count \
            FROM {config_dic['database']}.reads \
            GROUP BY consensus_id;",
                cnx,
            )
            .head()
            .to_dict(),
            {
                "seq_id": {0: 12, 1: 18, 2: 28, 3: 36, 4: 54},
                "duplicate_count": {0: 1, 1: 2, 2: 1, 3: 1, 4: 1},
            },
        )

        cursor.close()
        cnx.close()

    @classmethod
    def tearDownClass(cls):
        cnx = sr.connect(config_dic, ssh_group="test")
        this_dir, _ = os.path.split(__file__)
        test_dir = os.path.join(this_dir, "tmp_path")

        for db in ["test_main", "test_onto", "test_meta", "test_main_airr"]:

            cursor = cnx.cursor()

            # drop test databases
            try:
                cursor.execute(f"DROP DATABASE {db}")
                cnx.commit()
                cursor.close()
            except MySQLdb.Error:
                print(f"Database {db} does not exists. Dropping db failed")
        # delete temp dir
        try:
            shutil.rmtree(test_dir)
        except OSError as e:
            print("Error: %s : %s" % (test_dir, e.strerror))

        cnx.close()


test_dict = {
    "seq_id": {0: 58},
    "name": {0: "684"},
    "locus": {0: "H"},
    "orient": {0: "F"},
    "igblast_productive": {0: 1},
    "quality": {
        0: "100 100 100 100 100 100 100 100 100 68 68 68 68 68 68 68 68 68 68 68 68 68 68 68 68 68 68 68 68 68"
    },
    "event_id": {0: 2},
    "sort_id": {0: 1},
    "sample_id": {0: 1},
    "tissue": {0: "PBMC"},
    "add_sample_info": {0: ""},
    "donor_identifier": {0: "HD01"},
    "background_treatment": {0: ""},
    "strain": {0: ""},
    "add_donor_info": {0: "sex=male,age=30y"},
    "species_id": {0: "human"},
    "query_seq": {0: "GTGCAGCTGCTGGAGTCGGGGGGAGGCTT"},
    "population": {0: "IgG_memory"},
}

result_dict = {
    "name": "684",
    "locus": {"58": "H"},
    "orient": {"58": "F"},
    "igblast_productive": {"58": 1},
    "quality": {
        "58": "100 100 100 100 100 100 100 100 100 68 68 68 68 68 68 68 68 68 68 68 68 68 68 68 68 68 68 68 68 68"
    },
    "event_id": {"58": 2},
    "sort_id": {"58": "1"},
    "sample_id": {"58": 1},
    "tissue": {"58": "PBMC"},
    "add_sample_info": {"58": ""},
    "donor_identifier": {"58": "HD01"},
    "background_treatment": {"58": ""},
    "strain": {"58": ""},
    "add_donor_info": {"58": "sex=male,age=30y"},
    "species_id": {"58": "human"},
    "query_seq": {"58": "GTGCAGCTGCTGGAGTCGGGGGGAGGCTT"},
    "population": {"58": "IgG_memory"},
}


# simple unit test for pre_processing
@patch("pandas.read_sql")
def test_get_df(read_sql_mock: Mock):
    read_sql_mock.return_value = pd.DataFrame(test_dict)
    results = aex.pre_process_rearrangements(pd.DataFrame(test_dict))
    pd.testing.assert_frame_equal(
        results.reset_index(drop=True), pd.DataFrame(result_dict).reset_index(drop=True)
    )
