-- MariaDB dump 10.19  Distrib 10.4.19-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: scireptor_ontology
-- ------------------------------------------------------
-- Server version	10.4.19-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ontologies`
--

DROP TABLE IF EXISTS `ontologies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ontologies` (
  `ontology_id` varchar(100) NOT NULL,
  `label` varchar(100) DEFAULT NULL,
  `preferred_label` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ontologies`
--

LOCK TABLES `ontologies` WRITE;
/*!40000 ALTER TABLE `ontologies` DISABLE KEYS */;
INSERT INTO `ontologies` VALUES ('CL:0000236','B cell',1),('CL:0000786','plasma cell',1),('CL:0000788','naive B cell',1),('CL:0000819','CD19SP B cell',1),('CL:0000843','follicular B cell',1),('CL:0000844','germinal center B cell',1),('CL:0000845','marginal zone B cell',1),('CL:0000955','pre-B-II cell',1),('CL:0000958','Transitional 1 B cell',1),('CL:0000959','Transitional 2 B cell',1),('CL:0000979','IgG memory B cell',1),('CL:0002052','Fraction D precursor B cell',1),('CL:0002054','immature B cell',1),('CL:0002056','recirculating B cell',1),('CL:0002400','Fraction B/C precursor B cell',1),('NCBITAXON:10090','Mus musculus',1),('NCBITAXON:9606','Homo sapiens',1),('UBERON:0000029','lymph node',1),('UBERON:0000178','blood',1),('UBERON:0001179','peritoneal cavity',1),('UBERON:0002106','spleen',1),('UBERON:0002371','bone marrow',1),('UBERON:0002509','mesenteric lymph node',1),('UBERON:0003968','peripheral lymph node',1),('UO:0000033','day',1),('UO:0000034','week',1),('UO:0000036','year',1),('UO:0000033','d',0),('UO:0000034','weeks',0),('UO:0000034','w',0),('UO:0000034','wk',0),('UO:0000036','y',0),('UBERON:0002106','Spl',0),('NCBITAXON:9606','human',0),('NCBITAXON:10090','mouse',0),('UBERON:0000178','PBMC',0),('CL:0000819','B1 B cell',1),('CL:0000820','B1a B cell',1),('CL:0000821','B1b B cell',1),('CL:0000819','B1',0),('CL:0000820','B1a',0),('CL:0000821','B1b',0),('CL:0000979','IgG memory B cell',1),('CL:0000979','IgG_memory',0),('CL:0000905','CD4-positive effector memory T cell',1),('CL:0000905','TEM_CD4',0),('CL:0000236','B',0),('CL:0000786','PC',0),('CL:0000844','GC',0),('CL:0002054','immature',0),('CL:0000845','MZ',0),('CL:0002052','preB_CD25SP',0),('CL:0000955','preB_DN',0),('CL:0002400','preB_CD43SP',0),('CL:0002056','recirculating',0),('CL:0000958','T1',0),('CL:0000959','T2',0),('CL:0000819','CD19SP',0),('UBERON:0000029','LN',0),('CL:0000843','MN',0),('NCIT:C28278','Laboratory Study',1),('CL:00foo1','TFH_PD1++_ICOS+',1),('NCIT:C93130','Animal Study',1);
/*!40000 ALTER TABLE `ontologies` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-07-23  9:42:45
