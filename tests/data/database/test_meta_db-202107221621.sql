-- MariaDB dump 10.19  Distrib 10.4.19-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: scireptor_meta
-- ------------------------------------------------------
-- Server version	10.4.19-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `studies`
--

DROP TABLE IF EXISTS `studies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `studies` (
  `study_id` varchar(200) DEFAULT NULL,
  `study_title` varchar(2000) DEFAULT NULL,
  `study_type` varchar(2000) DEFAULT NULL,
  `inclusion_exclusion_criteria` varchar(2000) DEFAULT NULL,
  `grants` varchar(2000) DEFAULT NULL,
  `collected_by` varchar(2000) DEFAULT NULL,
  `lab_name` varchar(2000) DEFAULT NULL,
  `lab_address` varchar(2000) DEFAULT NULL,
  `submitted_by` varchar(2000) NOT NULL,
  `pub_ids` varchar(2000) DEFAULT NULL,
  `keywords_study` varchar(2000) NOT NULL,
  `study_description` varchar(500) DEFAULT NULL,
  `study_contact` varchar(100) DEFAULT NULL,
  `database_name` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `studies`
--

LOCK TABLES `studies` WRITE;
/*!40000 ALTER TABLE `studies` DISABLE KEYS */;
INSERT INTO `studies` VALUES ('3s238xt3478','test_main','NCIT:C93130',NULL,'SVP-2014-068289,SVP-2014-068216,SAF2016-75511-R,PGC2018-097019-B-I00,SEV-2015-0505','Christian Busse','Div. B Cell Immunology, German Cancer Research Center','Im Neuenheimer Field 280, 69120 Heidelberg, DE','Christian Busse',NULL,'contains_ig,contains_single_cell',NULL,NULL,'test_main');
/*!40000 ALTER TABLE `studies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'scireptor_meta'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-07-22 16:21:11
