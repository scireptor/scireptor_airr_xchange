## Scireptor AIRR exchange tool
**on development**

Scireptor AIRR exchange features a generic single-cell data import/export
toolkit for iReceptor Plus. Currently developed in parallel with the extension
of the MiAIRR data standard, the tool imports into the scireptor's backend database
a set of files proposed on [#409](https://github.com/airr-community/airr-standards/issues/409)
as the Common Exchange Format (**CEF**).
Validated with the `airr-schema.yaml` v 1.3, scireptor AIRR exchange provides
support for data imports from platforms compliant with the AIRR standard.

#### Requirements
The base Linux system used is **Fedora 32**

We assume that you are running the tool from a machine with access to the scireptorDB
and have a `$HOME/.my.cnf` configuration file,
if this is not the case please refer to the [scireptor documentation](https://gitlab.hzdr.de/scireptor/pipeline/-/blob/main/INSTALLATION.md#installing-and-running-scireptor) for instructions on how to set up the database access.

#### Install

Install required dependencies `sudo yum install python3-devel mysql-devel gcc`

clone this repository `cd` into `scireptor_airr_xchange` and run `pip install . --user`.


 #### Import usage

##### Import the five AIRR compliant files for single-cell RNA-seq indo the scireptorDB
In order to be able to import data into the scireptorDB you will need to have the five AIRR compliant files
for single-cell data exchange, file name prefixes are required by the tool:
```
cells_<projectname>.json
expression_<projectname>.tsv*
rearrangements_<projectname>.tsv
receptors_<projectname>.json
repertoires_<projectname>.json
```

To import the AIRR compliant files into the scireptorDB simply run:

 `airr_import -n <project_name> -i <input_path> -g <mariadb_group_name>`

**arguments**:

* `-h, --help `
show help message and exit
*  `-n PROJECT_NAME, --project_name`
project name of the scireptor database to import
*  `-g MARIADB_GROUP, --mariadb_group`
group name of the MariaDB on the `.my.cnf` config file with connection parameters
*  `-i INPUT_PATH, --input_path`
path to the folder with the AIRR files to be imported
*  `-s, --ssh`
use tunnel config for db connection (see below)


#### Export usage

##### AIRR compliant exporter for the scireptor databases.

Creates a `<database_name>` folder containing the five AIRR compliant files for single-cell RNA-seq as described on [#409](https://github.com/airr-community/airr-standards/issues/409) with the `airr-schema.yaml` v 1.3.


To generate the AIRR compliant files simply run:
`airr_export.py -d <database_name> -o <output_path> -g <mariadb_group_name>`

example:
 `airr_export.py -d healthy -o ~/exporter_example -g mysql_igdb`

**arguments**:

*  `-h, --help  `
show help message and exit
*  `-d DATABASE, --database`
name of the scireptor database to export
*  `-g MARIADB_GROUP, --mariadb_group`
name of the MariaDB group on the `.my.cnf` config file with connection parameters
*  `-o OUTPUT_PATH, --output_path`
path to the folder where the output will be stored
*  `-f INFO_FILE, --info_file`
path to the fastq.info file, if not file is provided D130 template will be used
*  `-filter_cells` (experimental)
if used, an interactive dialogue will prompt asking the user for a subset of cell populations to be exported:
```bash
Please enter the index(es) -space separated- of the populations you want to process:

        {0: 'GC', 1: 'PC'}
```
*  `-s, --ssh`
use tunnel config for db connection

----------------------------
to use the `-s` flag to connect using `sshtunnel`
create an [ssh] entry in your `~/.my.cnf` file with the connection parameters:
 ```
  [ssh]
  password=yourpassword
  user=username
  privatekey='path/to/private/key'
  ```
