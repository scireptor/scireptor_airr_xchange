import pandas as pd
from collections import OrderedDict
import itertools
from scirexport.logger import logger
from scirexport.sr_tools import query_receptor
from typing import List, Tuple

MySQLcon = str  # typing alias for the MySQL connection object
MySQLcur = str  # typing alias for the MySQL cursor object


def safe_create_schema(db: MySQLcon, project_name: str) -> None:
    """
    check for existing databases with the project name and abort import if database exists
    :param db: db connection object
    :param project_name: database name
    :return: None
    """
    # #set database and get db connection
    c = db.cursor()
    # abort if database exists
    if project_name in pd.read_sql("show databases", db).Database.tolist():
        db.close()
        logger.error(f"{project_name} is already in the database")
        db.close()
        raise NameError(f"Database `{project_name}` already exists")
    else:
        # Create database scheme
        c.execute(f"CREATE SCHEMA IF NOT EXISTS {project_name};")
        db.close()


# 1  receptors import function # todo adapt for TCR
def import_receptors(receptors_json: dict, db: MySQLcon, mode="") -> None:

    """
    :param mode: test
    :param receptor_type: type of receptor TCR|BCR
    :param receptors_json: receptors dictionary with format {id:{"H_seq":"IGH","L_seq":"IGL|IGK"}}
    :param db: database connection object
    :return: None, inserts new observed receptors into the `scireptor.receptors` database
    """

    if "test" in mode:
        receptors_db = "test_receptors"
    else:
        receptors_db = "scireptor_receptors"

    # normalize receptors dictionary
    receptors_dic = {}
    for x, v in receptors_json.items():
        receptors_dic[x] = {locus: seq for seq, locus in v.items()}
    # receptors DF

    logger.info("Receptors processing started")
    c = db.cursor()
    c.execute(
        f"SELECT receptor_id FROM `{receptors_db}`.`receptors` ORDER BY receptor_id DESC LIMIT 1;"
    )
    last_id = c.fetchone()  # get the last inserted id on the db

    if not last_id:
        last_id = [0]
    # counter for ids starting on the last inserted on the db
    receptor_id_count = itertools.count(int(last_id[0]) + 1)

    print_info_dic = {"found_receptors": [], "new_receptors": []}

    for receptor in receptors_dic:
        # look up for both positions in the receptors table to avoid inserting duplicates.
        this_list = [(receptors_dic[receptor][x], x) for x in receptors_dic[receptor]]
        combos = [(this_list[1] + this_list[0]), (this_list[0] + this_list[1])]

        for x in combos:

            found_receptor = query_receptor(c, x, mode)

            if found_receptor:
                print_info_dic["found_receptors"].append(found_receptor[0])
            else:

                fix_receptor_id = next(receptor_id_count)
                values_insert = (fix_receptor_id,) + x

                sql_insert = f"""INSERT INTO `{receptors_db}`.`receptors` (
                    `receptor_id`, `chain_1`, `chain_2`, `locus_1`, `locus_2`) VALUES (%s,%s,%s,%s,%s);"""
                c.execute(sql_insert, values_insert)
                print_info_dic["new_receptors"].append(fix_receptor_id)

    # print info of inserted receptors
    logger.info(
        f'{len(print_info_dic["new_receptors"])} new receptors have been inserted into the `receptors` db'
    )
    logger.info(
        f'{len(print_info_dic["found_receptors"])} receptors were already in the `receptors` db'
    )
    logger.info("[SUCCESS] Receptors data have been processed")


# 2  rearrangements DF import functions


def postprocess(
    rearrangements: pd.DataFrame, replace_columns: List[str]
) -> pd.DataFrame:
    """
    perform post processing on the dataframe to insert in the database
    :param rearrangements:
    :param replace_columns:
    :return:
    """
    return (
        rearrangements.assign(
            prot_length=lambda d: d["prot_seq"].str.len(),
            length=lambda d: d["sequence"].str.len(),
            consensus_rank=1,
            stop_codon=lambda d: d["prot_seq"].str.contains(
                "*", regex=False
            ),  # np.where(.., True, False)
            igblast_rank=0,  # zero or empty?
            seq=lambda d: d["sequence"],
            locus=lambda d: d["locus"].str[-1],
        )
        .replace({"igblast_productive": {"F": 0, "T": 1, "": 0, None: 0}})
        .replace({"stop_codon": {False: 0, True: 1}})
        .loc[:, lambda d: ~d.columns.duplicated()]
        .rename(columns=replace_columns)
    )


def unstack_vdj_cdr(
    df: pd.DataFrame, tuple_cols: List[Tuple], idx_col: str
) -> pd.DataFrame:
    """
    unstack VDJ and CDR
    :param df:
    :param tuple_cols:
    :param idx_col:
    :return:
    """
    df_to_stack = df[tuple_cols]
    df_to_stack.columns = pd.MultiIndex.from_tuples(
        df_to_stack.columns, names=["idx", idx_col]
    )
    stacked_df = df_to_stack.stack()
    stacked_df.reset_index(level=idx_col, inplace=True)
    return stacked_df


def import_rearrangements(
    rearrangements: pd.DataFrame, map_json: dict, project_name: str, db: MySQLcon
) -> None:
    """

    :param rearrangements: pandas dataframe from the AIRR compliant TSV rearrangements file
    :param map_json: json dictionary from the `airr_scireptor.json` file
    :param project_name: name of the db where data will be imported
    :param db: database connection object
    :return: imports rearrangements data into the `project_name` db
    """

    # define cursor
    c = db.cursor()

    airr_extended_dic = map_json

    # get columns that have been mapped
    rearrangement_cols = [x for x in rearrangements.columns if x in airr_extended_dic]

    logger.info("TSV Rearrangements file import started")

    # get list of mapped tables that will be inserted into the db
    table_list = set(
        [
            airr_extended_dic[x]["table"]
            for x in rearrangements.columns
            if x in airr_extended_dic
            and airr_extended_dic[x]
            and "table" in airr_extended_dic[x]
            and "current" == airr_extended_dic[x]["database"]
        ]
    )

    # dictionary of column names to replace with scireptor expected col names
    # replace columns to be stacked with tuples for multi-index
    replace_columns = {
        col_name: (
            (
                airr_extended_dic[col_name]["replace_name"],
                list(airr_extended_dic[col_name]["expanded_column"].values())[0],
            )
            if "expanded_column" in airr_extended_dic[col_name]
            else airr_extended_dic[col_name]["column"]
        )
        for col_name in rearrangement_cols
        if "column" in airr_extended_dic[col_name]
    }

    # set multi index tuple to replace column names
    set_tuple_replacement = set(
        [
            list(airr_extended_dic[col_name]["expanded_column"].values())[0]
            for col_name in rearrangement_cols
            if "expanded_column" in airr_extended_dic[col_name]
        ]
    )

    seq_cols = {"vdj_cols": [], "cdr_cols": []}
    for x in set_tuple_replacement:
        rearrangements[("sequence_id", x)] = rearrangements["sequence_id"]
        if x in ["V", "D", "J"]:
            seq_cols["vdj_cols"].append(("sequence_id", x))
        else:
            seq_cols["cdr_cols"].append(("sequence_id", x))

    # make replacements for only those columns to be stacked
    rearrangements.rename(
        columns={
            x: replace_columns[x]
            for x in replace_columns
            if type(replace_columns[x]) == tuple
        },
        inplace=True,
    )

    # set list of columns for the dfs to stack
    unstack_cols = {
        "vdj_cols": seq_cols["vdj_cols"]
        + [x for x in replace_columns.values() if type(x) == tuple and x[0] == "call"],
        # change to in vdj
        "cdr_cols": seq_cols["cdr_cols"]
        + [x for x in replace_columns.values() if type(x) == tuple and x[0] != "call"],
    }

    # create stacked dataframes for vdj and cdr tables
    stacked_vdj = unstack_vdj_cdr(rearrangements, unstack_cols["vdj_cols"], "type")
    stacked_cdr = unstack_vdj_cdr(rearrangements, unstack_cols["cdr_cols"], "region")
    # join the stacked dataframes with the original df
    stacked_df = stacked_cdr.merge(stacked_vdj, on="sequence_id")
    rearrangements = rearrangements.merge(stacked_df, on="sequence_id")
    # call post processing function
    rearrangements = postprocess(rearrangements, replace_columns)

    # main insert loop, iterates on the database tables and populates them accordingly
    for table in table_list:

        sql = f"DESCRIBE {project_name}.{table};"
        c.execute(sql)
        logger.debug(sql)
        fetch_cols = c.fetchall()
        # keep columns that are in the MySQL table
        fetched_cols = [x[0] for x in fetch_cols]
        insert_table = rearrangements[[x for x in rearrangements if x in fetched_cols]]

        for col in insert_table:
            if col == "CDR_FWR_id":
                continue
            if col not in insert_table.columns.tolist():
                insert_table[col] = ""

        # creating column list for insertion
        insert_table = (
            insert_table.astype(object)
            .where(pd.notnull(insert_table), None)
            .drop_duplicates()
        )
        cols = "`,`".join([str(i) for i in insert_table.columns.tolist()])

        # Insert DataFrame records one at a time.
        count = 0
        for i, row in insert_table.iterrows():

            sql = (
                f"INSERT IGNORE INTO {table} (`"
                + cols
                + "`) VALUES ("
                + "%s," * (len(row) - 1)
                + "%s)"
            )
            insert_tuple = tuple(row)

            if table == "CDR_FWR" and not row.dna_seq:  # ignore empty dna_seq rows
                continue
            if table == "VDJ_segments":
                count += 1  # add new id so that duplicated seq_ids can be inserted
                insert_tuple = (count,) + tuple(row)
                sql = (
                    f"INSERT IGNORE INTO `{table}` (`"
                    + "VDJ_segments_id`,`"
                    + cols
                    + "`) VALUES (%s, %s,"
                    + "%s," * (len(row) - 2)
                    + "%s)"
                )
            elif table == "sequences":
                # todo add quality , read direction from metadata
                row["name"] = None
                if not row["name"]:
                    continue
                    insert_tuple = tuple(row)
                # missing expected name (id) collides with VDJ_segments name, can be discarded

            elif table == "constant_segments":
                row["length"] = None
                insert_tuple = tuple(row)
                # missing length for constant segments is not the same as the calculated above
                if "name" not in row:
                    continue
                    insert_tuple = tuple(row)
            logger.debug(f"{sql} % {insert_tuple}")
            c.execute(sql, insert_tuple)
            # commit to save changes on DB
    db.commit()
    logger.info(
        f"[SUCCESS] Rearrangements data have been imported into the `{project_name}` database"
    )


# 3  repertoires import functions


def query_study(cursor: MySQLcur, database_name: str) -> str:
    """
    function to query for existing studies on the scireptor_meta.studies table
    :param cursor: mysqldb connection cursor
    :param database_name: study title
    :return: result of the query from cursor.fetchone()
    """

    if "test" in database_name:
        meta_table = "test_meta"
    else:
        meta_table = "scireptor_meta"

    query_ = f"""SELECT `database_name` FROM  `{meta_table}`.`studies` where
    (`study_title` = '%s');"""
    cursor.execute(query_ % database_name)
    found = cursor.fetchone()
    return found


def sample_donor_insert(
    table_name: pd.DataFrame,
    mapped_cols: dict,
    project_name: str,
    insert_dict: dict,
    table_cols: list,
    c: MySQLcon,
) -> None:
    """

    :param table_name: donor|sample
    :param mapped_cols: dictionary of columns mapped from the airr-scireptor.json file
    :param project_name: name of the project
    :param insert_dict: dictionary with the repertoire parameters
    :param table_cols: database column names for sample or donor tables queried from the scireptor db
    :param c: connection cursor
    :return: none, performs db insertions for donor and sample info
    """
    # dictionary to replace add_x_info columns
    replace_dic_to_str = {"'": "", "{": "", "}": "", ", ": ",", ": ": "="}

    if table_name == "sequencing_run":
        add_info_name = "sequencing"
    else:
        add_info_name = table_name
    # get sample db columns
    sample_donor_all = {
        mapped_cols[x]["column"]: x
        for x in mapped_cols
        if mapped_cols[x]["table"] == table_name
        and mapped_cols[x]["column"]
        and mapped_cols[x]["column"] != f"add_{add_info_name}_info"
    }
    # get items to be inserted in the add_{table_name}_info col
    add_info_dict = {
        x: insert_dict[x]
        for x in mapped_cols
        if mapped_cols[x]["column"]
        and mapped_cols[x]["column"] == f"add_{add_info_name}_info"
        and mapped_cols[x]["table"] == table_name
        and type(insert_dict[x]) != OrderedDict  # ignore for now nested add info todo
        # that is: sequencing_files
    }

    dict_to_insert = {
        x: (insert_dict[sample_donor_all[x]] if x in sample_donor_all else None)
        for x in table_cols
    }

    # convert add_info_dict to string and format as add_x_info
    str_info = str(add_info_dict)
    for repl, repwith in replace_dic_to_str.items():
        str_info = str_info.replace(repl, repwith)
    # convert donor id to integer todo check which donor id to take
    if table_name != "sequencing_run":
        dict_to_insert["donor_id"] = int(insert_dict["subject_id"][-1])
    dict_to_insert[f"add_{add_info_name}_info"] = str_info
    placeholders = ", ".join(["%s"] * len(dict_to_insert))
    columns = ", ".join(dict_to_insert.keys())
    sql = "INSERT INTO %s.%s (%s) Values (%s)" % (
        project_name,
        table_name,
        columns,
        placeholders,
    )
    try:
        c.execute(sql, tuple(dict_to_insert.values()))
    except c.IntegrityError:
        logger.debug(f"skipping duplicated entries {dict_to_insert.values()}")
    except c.OperationalError as e:  # catch MySQL
        logger.error(str(e))
        raise c.OperationalError


def import_repertoires(
    repertoires_json: dict, map_json: dict, project_name: str, db: MySQLcon
) -> str:
    """
    import repertoire yaml file into the scireptor database
    :param repertoires_json: AIRR repertoire JSON file
    :param map_json: scrireptor-airr JSON mapping dictionary
    :param project_name: project name to be inported on the db
    :param db: db connection object
    :return:
    """
    # query the db tables,get column names to fill

    if "test" in project_name:
        meta_table = "test_meta"
    else:
        meta_table = "scireptor_meta"

    study_cols = pd.read_sql(f"DESCRIBE {meta_table}.studies;", db).Field.tolist()
    sample_cols = pd.read_sql(f"DESCRIBE {project_name}.sample;", db).Field.tolist()
    donor_cols = pd.read_sql(f"DESCRIBE {project_name}.donor;", db).Field.tolist()
    seq_run_cols = pd.read_sql(
        f"DESCRIBE {project_name}.sequencing_run;", db
    ).Field.tolist()

    c = db.cursor()  # define cursor
    # parse repertoires
    logger.info("Repertoires file import started")

    for prop in repertoires_json["Repertoire"]:
        # check types of the first level yaml properties
        for k in prop.keys():

            if type(prop[k]) in [dict, OrderedDict]:
                if k == "study":  # info to insert on the study tables

                    prop[k] = {
                        s: (
                            prop[k][s]["id"] if type(prop[k][s]) == dict else prop[k][s]
                        )
                        for s in prop[k].keys()
                    }

                    if "study_id" in prop[k] and prop[k]["study_id"] is None:
                        prop[k]["study_id"] = ""
                    if "study_title" in prop[k]:
                        prop[k]["study_title"] = f"{project_name}"
                    if "keywords_study" in prop[k]:
                        prop[k]["keywords_study"] = ",".join(prop[k]["keywords_study"])

                    to_insert = {x: prop[k][x] for x in prop[k] if x in study_cols}
                    missing_from_sr = [x for x in prop[k] if x not in study_cols]
                    ins_rows = tuple(to_insert.values())
                    cols = "`,`".join([str(i) for i in to_insert])
                    # insert study info if does not exist
                    if query_study(c, prop[k]["study_title"]) is None:
                        #                     columns from our table
                        sql_insert = (
                            f"INSERT INTO `{meta_table}`.`studies` (`{cols}`) VALUES ("
                            + "%s," * (len(ins_rows) - 1)
                            + " %s)"
                        )
                        c.execute(sql_insert, ins_rows)
                    if missing_from_sr:
                        logger.warning(
                            "missing to insert in the study table: ", missing_from_sr
                        )

                elif k == "subject":
                    # dataframe to append with sample and donor

                    subject_dic = prop[k]
                    for x in subject_dic:
                        if type(subject_dic[x]) in [dict, OrderedDict]:
                            if "id" in subject_dic[x].keys():
                                subject_dic[x] = subject_dic[x]["id"]

                else:
                    logger.warning(f"{k, prop[k]}")

            elif type(prop[k]) == list:
                # parse sample tables to insert
                if (
                    k == "sample"
                ):  # AIRR sample contains scireptor sample, donor and run_sequencing tables
                    #                     if type(prop[k]) == list:
                    for sample in prop[k]:

                        for p in sample:
                            if type(sample[p]) in [dict, OrderedDict]:
                                if "id" in sample[p].keys():
                                    sample[p] = sample[p]["id"]
                        insert_dict = {**sample, **subject_dic}

                        mapped_cols = {
                            x: map_json[x]
                            for x in insert_dict.keys()
                            if x in map_json and map_json[x]
                        }
                        not_mapped_cols = [
                            x
                            for x in insert_dict.keys()
                            if x not in mapped_cols and ".label" not in x
                        ]
                        wont_import = ", ".join(not_mapped_cols)
                        logger.warning(
                            f"The field(s): '{wont_import}' are not in the mapping file and won`t be imported"
                        )  # todo change warning to missing important properties

                        sample_donor_insert(
                            "sample",
                            mapped_cols,
                            project_name,
                            insert_dict,
                            sample_cols,
                            c,
                        )
                        sample_donor_insert(
                            "donor",
                            mapped_cols,
                            project_name,
                            insert_dict,
                            donor_cols,
                            c,
                        )
                        sample_donor_insert(
                            "sequencing_run",
                            mapped_cols,
                            project_name,
                            insert_dict,
                            seq_run_cols,
                            c,
                        )

                else:
                    logger.warning(
                        f"The field(s): {prop[k]} are not in the mapping file and won`t be imported"
                    )
            else:
                if k == "repertoire_id":
                    pass
                else:
                    logger.error(f"no list no dic {k} {prop[k]}, {type(prop[k])}")
                    logger.error(
                        f"{k} is not a valid property of the AIRR metadata yaml file"
                    )

    logger.info(
        f"[SUCCESS] YAML Repertoires file has been imported into the `{project_name}` and `studies` databases"
    )

    db.commit()


def load_normalize_expression(path_to_sparse_mx: str) -> pd.DataFrame:
    """

    :param path_to_sparse_mx: path to 10X gzipped TSV sparse matrix
    :return: normalized pandas dataframe to be imported into the db
    """
    expression_mx = pd.read_csv(
        path_to_sparse_mx, compression="gzip", sep="\t", index_col=0
    ).T

    stacked_mx = (
        expression_mx.stack()
        .reset_index()
        .rename(columns={"level_0": "CELL", "level_1": "GENE", 0: "LEVEL"})
    )
    return stacked_mx[stacked_mx.LEVEL != 0]


def import_10X_expression(
    path_to_sparse_mx: str, project_name: str, db: MySQLcon
) -> None:
    """

    :param path_to_sparse_mx: path to 10X gzipped TSV sparse matrix
    :param project_name: name of the db where data will be imported
    :param db: database connection object
    :return: imports 10X expression data into the `project_name` db
    """

    expression_df = load_normalize_expression(path_to_sparse_mx)

    logger.info("10X expression import started, this might take a while...")

    c = db.cursor()

    for i, row in expression_df.iterrows():
        sql = (
            f"INSERT IGNORE INTO `{project_name}`.expression (`"
            + "CELL_ID`, `GENE`, `LEVEL"
            + "`) VALUES ("
            + "%s," * (len(row) - 1)
            + "%s)"
        )
        insert_tuple = tuple(row)
        c.execute(sql, insert_tuple)
    db.commit()

    logger.info(
        f"[SUCCESS] 10X expression data have been imported into the `{project_name}` database"
    )


def import_FACS_expression(
    path_to_flow_tsv: str, project_name: str, db: MySQLcon
) -> None:
    """
    :param path_to_flow_tsv: path to FACS expression file
    :param project_name: name of the db where data will be imported
    :param db: database connection object
    :return: imports flow data into the `project_name` db
    """

    flow_hdf = pd.read_csv(path_to_flow_tsv, sep="\t")

    # set cursor
    c = db.cursor()
    flow_hdf.set_index("cell_id", inplace=True)
    # melt (akk. unstack) marker names and values by cell_id from the hdf5 dataframe
    flow_df = (flow_hdf.melt(ignore_index=False).reset_index()).dropna(
        subset=["value", "variable"]
    )
    # create a fake channel_id for the flow_meta table
    channel_ids = {x: i for i, x in enumerate(flow_df.variable.unique().tolist(), 1)}
    flow_df["channel_id"] = flow_df["variable"].map(channel_ids)

    # insert flow data in tables flow and flow_meta
    logger.info("Flow cytometry data import started")
    for i, row in flow_df.iterrows():

        sql = f"INSERT IGNORE INTO {project_name}.flow_meta (`channel_id`,`marker_name`) VALUES (%s,'%s')"
        c.execute(sql % (row["channel_id"], row["variable"]))
        sql = f"INSERT IGNORE INTO {project_name}.flow (`event_id`, `value`, `channel_id`) VALUES (%s,%s,%s)"
        c.execute(sql % (row["cell_id"], row["value"], row["channel_id"]))

    logger.info(
        f"[SUCCESS] Flow cytometry data have been imported into the `{project_name}` database"
    )

    db.commit()
