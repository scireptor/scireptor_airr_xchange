#!/usr/bin/env python3

"""
Author
git@franasa - f.arcila@dkfz-heidelberg.de


Description
Main script of the scirexport exporter,
generates five AIRR 1.3-4 compliant files based on the 409 discussion
https://github.com/airr-community/airr-standards/issues/409

Output files:

- rearrangements_{database}_AIRR.tsv
- repertoires_{database}_AIRR.yaml
- expression_data_{database}.h5
- cells_{database}_AIRR.json
- receptors_{database}_AIRR.json

"""

import scirexport.sr_tools as sr
import pandas as pd
import airr
import json
import os
import argparse
from argparse import RawTextHelpFormatter
from scirexport.logger import logger


# it's safe to deactivate SettingWithCopyWarning, Type I error
pd.options.mode.chained_assignment = None

# define args
parser = argparse.ArgumentParser(
    description=__doc__, formatter_class=RawTextHelpFormatter
)
parser.add_argument("-d", "--database", help="name of the scireptor database to export")
parser.add_argument(
    "-g",
    "--mariadb_group",
    help="name of the MariaDB group on the `.my.cnf` config file with connection parameters",
)
parser.add_argument(
    "-o", "--output_path", help="path to the folder where the output will be stored"
)
parser.add_argument(
    "-f",
    "--info_file",
    help="path to the fastq.info file, if not file is provided D130 template will be used",
)
parser.add_argument(
    "-s",
    "--ssh_group",
    help="name of the MariaDB group on the `.my.cnf` config file with connection parameters",
)

parser.add_argument(
    "-filter_cells",
    action="store_true",
    help="ask for user input to select a subset of cells from sort.population",
)

validate_airr_dic = sr.validate_airr_dic

args = parser.parse_args()

DB_ARGS = {
    "database": args.database,
    "config_db": args.mariadb_group,
    "info": args.info_file,
    "ssh_group": args.ssh_group,
    "filter_cells": args.filter_cells,
    "output_path": args.output_path,
}


def main():
    """
    Main function of the AIRR exporter for sciReptor.
    Exports the five AIRR compliant files for single cell on the specified folder
    for a given sciReptor database.
    Performs some basic compliance and formatting checks, sufficient to pass
    the AIRR validation routines.

    """
    # get variables
    database = DB_ARGS["database"]
    config_db = DB_ARGS["config_db"]
    mode = ""

    # get the template fastaq.info file with the D130 standard metadata annotations into a Df
    # todo change to use metadata information from the new sciReptor table
    if DB_ARGS["info"]:
        meta_template = sr.parse_meta_template(DB_ARGS["info"])
    else:
        meta_template = sr.parse_meta_template()

    # #set database and get db connection
    if DB_ARGS["ssh_group"] and "test" not in DB_ARGS["ssh_group"]:
        try:
            server, db = sr.connect(conf_db=config_db, ssh_group=DB_ARGS["ssh_group"])
        except Exception as error:
            logger.error("ERROR", error)

    elif DB_ARGS["ssh_group"] and "test" in DB_ARGS["ssh_group"]:
        mode = "test"
        db = sr.connect(conf_db=config_db, ssh_group=DB_ARGS["ssh_group"])
    else:
        db = sr.connect(conf_db=config_db, ssh_group=DB_ARGS["ssh_group"])

    (
        study_frame,
        replace_ontologies,
        read_counts,
        cell_counts,
        pre_cdr_fwr_df,
        pre_vdj_df,
        flow_frame,
        rearrangements_frame,
        receptor_type,
        out_path,
    ) = pd_sql_queries(database, db, DB_ARGS["filter_cells"], DB_ARGS["output_path"])

    pre_rearrangements_df = pre_process_rearrangements(rearrangements_frame)
    repertoires_dict = pre_process_repertoires(
        pre_rearrangements_df, cell_counts, receptor_type
    ).pipe(
        process_repertoires,
        replace_ontologies,
        study_frame,
        meta_template,
    )

    rearrangements_df = process_rearrangements(
        pre_rearrangements_df,
        read_counts,
        receptor_type,
        pre_cdr_fwr_df,
        pre_vdj_df,
        replace_ontologies,
    )
    rearrangements, receptors = sr.receptor_id_assign(
        rearrangements_df, receptor_type, db, mode
    )
    # Empty columns added to pass airr validation
    rearrangements = pd.concat(
        [
            rearrangements,
            pd.DataFrame(
                columns=[
                    "rev_comp",
                    "sequence_alignment",
                    "germline_alignment",
                    "v_cigar",
                    "d_cigar",
                    "j_cigar",
                ]
            ),
        ]
    ).pipe(set_airr_formats)

    flow_array = process_flow_data(flow_frame)
    cell_dict = process_cells(rearrangements_df)

    export_files(
        out_path,
        database,
        repertoires_dict,
        rearrangements,
        receptors,
        cell_dict,
        flow_array,
    )

    db.close()

    if DB_ARGS["ssh_group"] and "test" not in DB_ARGS["ssh_group"]:
        server.stop()

    return True


def pd_sql_queries(database, db, filter_cells, output_path, test=False):

    if test or database == "test_main":
        meta_table = "test_meta"
        onto_table = "test_onto"
    else:
        meta_table = "scireptor_meta"
        onto_table = "scireptor_ontology"
        db = db[1] if type(db) == list else db

    if database not in pd.read_sql("show databases", db).Database.tolist():
        logger.error(f"Database {database} does not exist")
        raise NameError(f"Database {database} does not exist")

    # query the study table and make a dic
    study_frame = pd.read_sql(
        f"SELECT * FROM {meta_table}.studies WHERE database_name = '{database}'", db
    ).to_dict("records")[0]
    # load ontologies table for curies replacement
    ontologies_df = pd.read_sql(f"SELECT * FROM  {onto_table}.ontologies;", db)
    replace_ontologies = {x["label"]: x for x in ontologies_df.to_dict("records")}
    # query read_counts per consensus_id akk seq_id
    read_counts = pd.read_sql(
        f"SELECT reads.consensus_id as seq_id ,COUNT(DISTINCT(seq_id)) as duplicate_count \
        FROM {database}.reads \
        GROUP BY consensus_id;",
        db,
    )

    # query cell counts per repertoire_id akk sort_id
    cell_counts = pd.read_sql(
        f"SELECT event.sort_id, \
        COUNT(DISTINCT(event.event_id)) AS cell_number FROM {database}.event \
        JOIN {database}.sort ON event.sort_id = sort.sort_id GROUP BY sort_id;",
        db,
    )

    # query CDR_FWR table to unstack the translated sequences
    pre_cdr_fwr_df = pd.read_sql(f"SELECT CDR_FWR.* FROM {database}.CDR_FWR;", db)
    # query VDJ_segments table to unstack the translated sequences and get first rank
    pre_vdj_df = pd.read_sql(
        f"SELECT VDJ_segments.* FROM {database}.VDJ_segments WHERE igblast_rank = '1';",
        db,
    )

    # get expression values
    flow_frame = pd.read_sql(
        f"SELECT event_id, value, detector_name, coalesce(marker_name, detector_name) \
    AS marker_name FROM {database}.flow \
    join {database}.flow_meta on flow.channel_id= flow_meta.channel_id;",
        db,
    )

    if "contains_ig" in study_frame["keywords_study"]:
        receptor_type = "IG"
    elif "contains_tcr" in study_frame["keywords_study"]:
        receptor_type = "TCR"
    else:
        raise ValueError(
            "Could not determine type of receptor from the controlled vocabulary in 'keywords_study'"
        )

    # this SQL statement is necessary to create get the sequencing_run_id's for each repertoire tobe improved
    rearrangements_query = f"SELECT sequences.*, event.*, \
        sample.*, donor.*, igblast_alignment.*, sequencing_run.*, \
        constant_segments.name as constant_name,\
        donor.*, sort.* FROM {database}.event \
        JOIN {database}.sequences ON sequences.event_id = event.event_id \
        JOIN {database}.reads ON reads.consensus_id = sequences.seq_id \
        JOIN {database}.sort ON sort.sort_id = event.sort_id %s\
        JOIN {database}.sequencing_run ON sequencing_run.sequencing_run_id = reads.sequencing_run_id \
        JOIN {database}.sample ON sample.sample_id = sort.sample_id \
        JOIN {database}.igblast_alignment ON igblast_alignment.seq_id = sequences.seq_id \
        JOIN {database}.constant_segments ON constant_segments.seq_id = sequences.seq_id \
        JOIN {database}.donor ON donor.donor_id = sample.donor_id;"

    if filter_cells:
        params_keys, string_reps = input_cell_types(database, db)

        params = {
            "sql": rearrangements_query % string_reps,
            "con": db,
            "params": params_keys,
        }
        out_path = f"{output_path}/{database}_fragment_{'_'.join(params_keys)}"
    else:
        params = {"sql": rearrangements_query % "", "con": db}
        out_path = f"{output_path}/{database}"
    rearrangements_frame = pd.read_sql_query(**params)

    return (
        study_frame,
        replace_ontologies,
        read_counts,
        cell_counts,
        pre_cdr_fwr_df,
        pre_vdj_df,
        flow_frame,
        rearrangements_frame,
        receptor_type,
        out_path,
    )


def input_cell_types(database: str, db) -> str:

    list_population = pd.read_sql(
        f"SELECT sort.population FROM {database}.sort;", db
    ).population.tolist()
    dict_population = {list_population.index(x): x for x in list_population}

    """ function for the filter_cells flag, choose a set of sort.population to be exported as a repertoire"""

    while True:
        filter_population = input(
            f"""Please enter the index(es) (space separated) of the populations you want to process:

        {dict_population}

        """
        )
        not_valid = [
            x for x in filter_population.split() if int(x) not in dict_population
        ]
        if not_valid:
            print(
                f"ERROR: {not_valid} is not a population index, available indexes are: {dict_population.keys()}"
            )
        else:
            selection_dic = {
                dict_population[int(x)]: x for x in filter_population.split()
            }
            break
    if len(selection_dic) == 1:
        sql_string = "AND sort.population = %s"
    elif len(selection_dic) == 2:
        sql_string = "AND sort.population IN (%s, %s)"
    elif len(selection_dic) > 2:
        sql_string = (
            "AND sort.population IN (%s, " + "%s, " * (len(selection_dic) - 2) + "%s)"
        )
    return selection_dic.keys(), sql_string


def start_pd_pipe(df: pd.DataFrame) -> pd.DataFrame:
    """
    Start all pipe processes with a clean copy of the dataframe
    :returns df copy"""
    return df.copy()


def set_data_types(df: pd.DataFrame, index_name: str) -> pd.DataFrame:
    """

    :param df: input dataframe
    :param index_name: column name to be indexed
    :return: dataframe with basic data types transformations
    """
    formatting_dic = {
        x: z
        for x, z in {
            "sort_id": "str",
            "seq_id": "str",
            "sequencing_run_id": "str",
        }.items()
        if x in df.columns
    }

    df = (
        df.dropna(subset=[index_name])
        .loc[:, ~df.columns.duplicated()]
        .drop_duplicates()
        .astype({index_name: int})
        .astype(formatting_dic)
    )
    return df


def chain_to_vdj_df(midf: pd.DataFrame) -> pd.DataFrame:
    """

    :param midf: a multi-index DF to be normalized
    :return: data frame with normalized columns joined with an underscore
    """
    midf.columns = midf.columns.map("_".join).str.strip("_")
    return midf.reset_index()


def process_vdj_df(df_vdj: pd.DataFrame) -> pd.DataFrame:
    """

    :param df_vdj:  the VDJ dataframe from the mysql query
    :return: clean normalized vdj dataframe to be joined with the rearrangements
    """
    return (
        df_vdj.pipe(start_pd_pipe)
        .pipe(set_data_types, "seq_id")
        .set_index(["seq_id", "type"])
        .unstack("type")
        .pipe(chain_to_vdj_df)
        .set_index("seq_id")
    )


def chain_to_cdr_fwr_df(midf: pd.DataFrame) -> pd.DataFrame:
    """

    :param midf: a multi-index DF to be normalized
    :return:  data frame with normalized columns joined with an underscore
    additionally adding a stop_codon column for the complete sequence
    """
    midf.columns = midf.columns.map("_".join).str.strip("_")
    # add total stop codon per sequence
    midf["stop_codon"] = midf[[x for x in midf.columns if "stop_codon" in x]].sum(
        axis=1
    )
    midf["stop_codon"] = [True if x >= 1 else False for x in midf["stop_codon"]]
    return midf


def rem_duplicates(df: pd.DataFrame) -> pd.DataFrame:
    """

    :param df:dataframe to remove duplicates
    :return:
    """
    return df[~df.index.duplicated(keep="first")]


def process_cdr_fwr_df(df_cdr_fwr: pd.DataFrame, receptor_type: str) -> pd.DataFrame:
    """
    :param df_cdr_fwr: the CDR_FWR dataframe  from the mysql query
    :param receptor_type: type of receptor inferred from the studies table name
    :return: normalized dataframe with `junction_aa` and `junction` columns required by the standard
    """
    return (
        df_cdr_fwr.pipe(start_pd_pipe)
        .pipe(set_data_types, "seq_id")
        .set_index(["seq_id", "region"])
        .pipe(rem_duplicates)
        .unstack("region")
        .pipe(chain_to_cdr_fwr_df)
        .pipe(sr.airrize_df, receptor_type, validate_airr_dic)
        # added to fulfill IMGT junction_aa definitions
        .assign(
            junction_aa=lambda d: d["fwr3_aa"].str[-1]
            + d["cdr3_aa"]
            + d["fwr4_aa"].str[0],
            junction=lambda d: d["fwr3"].str[-3] + d["cdr3"] + d["fwr4"].str[:3],
        )
    )


def format_id_val_ontologies(meta_dict: dict, ontologies_dic: dict) -> None:
    """

    :param meta_dict: input repertoires dictionary to be AIRR-ized
    :param ontologies_df: dataframe from mysql query with the mapped ontologies
    :return: converts annotated ontology fields to a {id: label:}
    """

    preferred_labels = {
        ontologies_dic[x]["ontology_id"]: ontologies_dic[x]["label"]
        for x in ontologies_dic
        if ontologies_dic[x]["preferred_label"] == 1
    }

    fix_ontologies = {}

    for x in meta_dict:
        if x in validate_airr_dic and "Ontology" in validate_airr_dic[x]["Format"]:
            if meta_dict[x] not in preferred_labels:
                logger.warning(f"WARNING {x} not in ontologies table")
                fix_ontologies[x] = {"id": None, "label": None}
            elif meta_dict[x] in preferred_labels:
                fix_ontologies[x] = {
                    "id": meta_dict[x],
                    "label": preferred_labels[meta_dict[x]],
                }
            else:
                print(x)

    for x in meta_dict:
        if x in fix_ontologies:
            meta_dict[x] = fix_ontologies[x]

    return meta_dict


def pre_process_repertoires(
    df_rearrangements: pd.DataFrame,
    cell_counts: pd.DataFrame,
    receptor_type: str,
) -> pd.DataFrame:
    """

    :param df_rearrangements: rearrangements df from the mysql query
    :param cell_counts: cell count df from the mysql query
    :param receptor_type: type of receptor inferred from the studies table name
    :return: pre processed repertoires df joined with cell counts
    and unwrapped add*info columns
    """

    cell_counts_df = (
        cell_counts.pipe(start_pd_pipe)
        .pipe(set_data_types, "sort_id")
        .rename(columns={"sort_id": "repertoire_id"})
        .set_index("repertoire_id")
    )

    # minimal columns required by AIRR
    min_repertoire = [
        x
        for x in validate_airr_dic.keys()
        if "Repertoire" in validate_airr_dic[x]["File"]
        and validate_airr_dic[x]["Level"] != ""
    ]

    pre_df = (
        df_rearrangements.pipe(sr.unwrap_add_info)
        .pipe(sr.airrize_df, receptor_type, validate_airr_dic)
        .sort_values(by=["repertoire_id"])
        .drop_duplicates(
            subset=["repertoire_id", "sequencing_run_id"], keep="last", inplace=False
        )
        .set_index("repertoire_id")
        .replace("nan", None)
    )

    return pre_df[[x for x in min_repertoire if x in pre_df.columns]].merge(
        cell_counts_df, left_index=True, right_index=True
    )


def process_repertoires(
    repertoires_df: pd.DataFrame,
    replace_ontologies: dict,
    study_frame: dict,
    meta_template: dict,
) -> dict:
    """

    :param repertoires_df: output df of the pre_process_repertoires function
    :param replace_ontologies: dictionary of the ontologies that will be replaced with the ontologies df
    :param study_frame: df from the mysql query with the study table information
    :param meta_template: template df for the metadata information that is general for all sciReptor dbs
    :return: dictionary of repertoires to be dumped as an AIRR compliant yaml file
    """
    # get lists of grouped sequencing_run _id and _date

    run_id_dic = (
        repertoires_df.groupby("repertoire_id")["sequencing_run_id"]
        .apply(", ".join)
        .to_dict()
    )
    repertoires_df["sequencing_run_date"] = repertoires_df["sequencing_run_date"]
    repertoires_df["sample_id"] = repertoires_df["sample_id"]
    repertoires_df["cell_species"] = repertoires_df["species"]

    run_date_dic = (
        repertoires_df.groupby("repertoire_id")["sequencing_run_date"]
        .apply(", ".join)
        .to_dict()
    )

    repertoires_noduplicates = repertoires_df[
        ~repertoires_df.index.duplicated(keep="first")
    ].pipe(sr.validate_columns, replace_ontologies)

    repertoire_dic = {}

    for i, row in repertoires_noduplicates.iterrows():

        row["repertoire_id"] = f"{study_frame['database_name']}_{i}"
        row[
            "data_processing_files"
        ] = f"rearrangements_{study_frame['database_name']}_{i}_AIRR.tsv"
        row["cell_processing_id"] = i  # cell processing id is for now repertoire_id
        row["sequencing_run_date"] = run_date_dic[i]
        row["sequencing_run_id"] = run_id_dic[i]
        meta_template["cells_per_reaction"] = int(meta_template["cells_per_reaction"])

        repertoire_dic[i] = {**row.to_dict(), **study_frame, **meta_template}

    list_reps = []
    for rep in repertoire_dic:

        # change floats to integers
        repertoire_dic[rep] = {
            x: None
            if pd.isna(repertoire_dic[rep][x]) or repertoire_dic[rep][x] == ""
            else int(repertoire_dic[rep][x])
            if type(repertoire_dic[rep][x]) == float
            else repertoire_dic[rep][x]
            for x in repertoire_dic[rep]
        }
        rept = sr.dump_repertoire(
            format_id_val_ontologies(repertoire_dic[rep], replace_ontologies)
        )

        list_reps.append(rept)

    essential_validation(repertoire_dic[rep], rep)

    import airr

    try:
        airr.RepertoireSchema.validate_object(rept)
    except airr.ValidationError as e:
        print("has repertoire at array position with validation error: %s\n" % e)

    repertoires_dic = {"Repertoire": list_reps}
    return repertoires_dic


def essential_validation(repertoire_in: dict, repertoire_id: int = 0) -> None:
    """

    :param repertoire_in: dataframe of a repertoire to be validated
    :param repertoire_id: current repertoire_id for warning message
    :return: None logs warning and info for expected AIRR properties
    """
    min_repertoire = [
        x
        for x in validate_airr_dic.keys()
        if "Repertoire" in validate_airr_dic[x]["File"]
        and validate_airr_dic[x]["Level"] != ""
    ]

    min_sc_rearrangements = [
        x
        for x in validate_airr_dic.keys()
        if "Rearrangement" in validate_airr_dic[x]["File"]
        and validate_airr_dic[x]["Level"] != ""
    ]

    if type(repertoire_in) == pd.DataFrame:
        eval_input = repertoire_in.columns
        eval_list = min_sc_rearrangements
        repertoire_id = ""
        warning_msg = (
            "Essential MiAIRR properties missing for the rearrangements TSV: \n {}{}\n"
        )
        info_msg = "Important MiAIRR properties are missing for the rearrangements TSV \n {}{}\n"

    elif type(repertoire_in) == dict:
        eval_input = repertoire_in
        eval_list = min_repertoire
        warning_msg = (
            "Essential MiAIRR properties missing for repertoire_id={}: \n {}\n"
        )
        info_msg = (
            "Important MiAIRR properties are missing for repertoire_id={}: \n {}\n"
        )

        # print warnings
    if "essential" in [
        validate_airr_dic[x]["Level"] for x in eval_list if x not in eval_input
    ]:

        missing = str(
            [
                x
                for x in eval_list
                if x not in eval_input and validate_airr_dic[x]["Level"] == "essential"
            ]
        )
        logger.warning(warning_msg.format(repertoire_id, missing))
    else:
        missing = str(
            [
                x
                for x in eval_list
                if x not in eval_input and validate_airr_dic[x]["Level"] != "essential"
            ]
        )
        logger.info(info_msg.format(repertoire_id, missing))


def pre_process_rearrangements(rearrangements_frame: pd.DataFrame) -> pd.DataFrame:
    """
    preprocess
    remove duplicated sequences needed to calculate read counts
    as well as duplicated columns
    :param rearrangements_frame: rearrangements df from the mysql query
    :return: preprocessed rearrangements dataframe
    """
    # remove the zillion duplicates created by the sequencing_run_id JOIN
    df_rearrangements = rearrangements_frame.loc[
        :, ~rearrangements_frame.columns.duplicated()
    ].set_index("seq_id")
    df_rearrangements = df_rearrangements[
        ~df_rearrangements.index.duplicated(keep="first")
    ].reset_index()

    return (
        df_rearrangements.pipe(start_pd_pipe)
        .pipe(set_data_types, "seq_id")
        .set_index("seq_id")
    )


def process_rearrangements(
    df_pre_processed: pd.DataFrame,
    read_counts: pd.DataFrame,
    receptor_type: str,
    pre_cdr_fwr_df: pd.DataFrame,
    pre_vdj_df: pd.DataFrame,
    replace_ontologies: dict,
) -> pd.DataFrame:
    """

    :param df_pre_processed: pre processed rearrangements df
    :param read_counts: read counts df from the mysql query
    :param receptor_type: type of receptor inferred from the studies table name
    :param pre_cdr_fwr_df: CDR_FWR df from the mysql query
    :param pre_vdj_df: VDJ df from the mysql query
    :param replace_ontologies: dictionary of the ontologies that will be replaced with the ontologies df
    :return: processed rearrangements df that will be the input of the receptors assign function
    """
    # list of the information for a full AIRR single cell rearrangements annotation
    airr_full_sc_rearrangements = [
        x
        for x in validate_airr_dic.keys()
        if validate_airr_dic[x]["File"] != "Repertoire"
    ]

    concat_cols = [
        "fwr1_aa",
        "cdr1_aa",
        "fwr2_aa",
        "cdr2_aa",
        "fwr3_aa",
        "cdr3_aa",
        "fwr4_aa",
    ]

    read_counts_df = (
        read_counts.pipe(start_pd_pipe)
        .pipe(set_data_types, "seq_id")
        .set_index("seq_id")
    )

    process_df = (
        (
            pd.concat(
                [
                    df_pre_processed,
                    read_counts_df,
                    process_cdr_fwr_df(pre_cdr_fwr_df, receptor_type),
                    process_vdj_df(pre_vdj_df),
                ],
                join="inner",
                axis=1,
            )
            .pipe(sr.airrize_df, receptor_type, validate_airr_dic)
            .replace({"nan": None})
            .dropna(subset=concat_cols)
            .reset_index()
        )
        .assign(
            sequence_id=lambda d: d["seq_id"],
            sequence_aa=lambda d: d[concat_cols].apply(
                lambda row: "".join(row.values.astype(str)), axis=1
            ),
            quality=lambda d: d["quality"].apply(
                lambda row: " ".join(["40" for x in row.split(" ")])
            ),  # 40 added for consensus qualities
        )
        .pipe(sr.validate_columns, replace_ontologies)
    )

    cols_airr = [x for x in airr_full_sc_rearrangements if x in process_df.columns]
    essential_validation(process_df[cols_airr])
    return process_df[cols_airr]


def process_flow_data(flow_frame: pd.DataFrame) -> pd.DataFrame:
    """

    :param flow_frame: flow dataframe from the mysql queries
    :return: flow dataframe to be stored in the hd5 file
    """
    # prepare flow expression frame
    for event in flow_frame.event_id.unique().tolist():
        """added to create a new name when marker_name was found duplicated
        merges  marker_ and detector_name for the new name."""
        df = flow_frame[flow_frame.event_id == event]
        a = df.index.to_series().groupby(df.marker_name).apply(list)

        if [x for x in a if len(x) > 1]:
            loc_change = [x for x in a if len(x) > 1][0][1]
            flow_frame.loc[
                loc_change, "marker_name"
            ] = f"{df.loc[loc_change, 'detector_name']}_{df.loc[loc_change, 'marker_name']}"

    flow_array = (
        (
            flow_frame.pivot(index="event_id", columns="marker_name", values="value")
            .reset_index()
            .rename(columns={"event_id": "cell_id"})
        )
        .set_index("cell_id")
        .rename_axis(None, axis=1)
    )

    return flow_array


# create dictionary for the cell file. so far only has virtual = F


def process_cells(df_rearrangements: pd.DataFrame) -> dict:
    """

    :param df_rearrangements: processed rearrangements df
    :return: {cell_id: {"virtual": "F"}} dictionary to be dumped as json file
    """
    df_rearrangements["virtual_pairing"] = "F"
    cells_dict = df_rearrangements[
        ["cell_id", "repertoire_id", "virtual_pairing"]
    ].to_dict("records")

    return cells_dict


def set_airr_formats(airr_rearrangements: pd.DataFrame) -> pd.DataFrame:
    """

    :param airr_rearrangements: AIRR-ized rearrangements df
    :return: a dataframe with the formats sufficient to pass the AIRR validation tool
    """
    formats_dict = {
        ":ref:`Ontology <OntoVoc>`": "str",
        ":ref:`RawSequenceData <RawSequenceDataFields>`": "str",
        ":ref:`Study <StudyFields>`": "str",
        ":ref:`Subject <SubjectFields>`": "str",
        "array": "str",
        "array of :ref:`DataProcessing <DataProcessingFields>`": "str",
        "array of :ref:`Diagnosis <DiagnosisFields>`": "str",
        "array of :ref:`PCRTarget <PCRTargetFields>`": "str",
        "array of object": "str",
        "array of string": "str",
        "boolean": "bool",
        "integer": "int",
        "number": "int",
        "object": "str",
        "string": "str",
    }

    format_change = {
        x: formats_dict[validate_airr_dic[x]["Type"]]
        for x in validate_airr_dic
        if x in airr_rearrangements.columns
    }

    ins_cols = [
        x
        for x in airr_rearrangements.columns
        if x in format_change and format_change[x] == "int"
    ]

    airr_rearrangements[ins_cols] = airr_rearrangements[ins_cols].fillna(value=0)

    return airr_rearrangements.astype(format_change)


def export_files(
    base_path: str,
    database: str,
    repertoires_dic: dict,
    rearrangements_df: pd.DataFrame,
    receptors_dic: dict,
    cells_dic: dict,
    flow_df: pd.DataFrame,
) -> None:
    """

    :param base_path: output path where the files will be saved
    :param database: name of the database to be exported
    :param repertoires_dic: repertoires dictionary to be dumped as YAML
    :param rearrangements_df: rearrangements df to be saved as TSV
    :param receptors_dic: receptors dictionary to be dumped as JSON
    :param cells_dic: cells dictionary to be stored as JSON
    :param flow_df: flow dataframe to be stored in HD5 format
    :return: None
    """
    # create path if not exist
    if not os.path.exists(base_path):
        os.makedirs(base_path)
    # repertoires file

    airr.write_repertoire(
        f"{base_path}/repertoires_{database}_AIRR.json", repertoires_dic["Repertoire"]
    )

    # receptors file
    with open(f"{base_path}/receptors_{database}_AIRR.json", "w") as outfile:
        json.dump(receptors_dic, outfile, indent=4)
    # cell file
    with open(f"{base_path}/cells_{database}_AIRR.json", "w") as outfile:
        json.dump(cells_dic, outfile, indent=4)

    for (
        repertoire
    ) in (
        rearrangements_df.repertoire_id.unique().tolist()
    ):  # export individual TSV repertoires

        single_rep = rearrangements_df[rearrangements_df["repertoire_id"] == repertoire]
        # single_rep.drop(columns=["repertoire_id"], inplace=True)

        if single_rep.empty:
            logger.warning(f"WARNING Repertoire {repertoire} is empty")

        else:
            single_rep.to_csv(
                f"{base_path}/rearrangements_{database}_{repertoire}_AIRR.tsv",
                index=False,
                sep="\t",
            )
    # export flow data/expression data
    flow_df.reset_index().to_csv(
        f"{base_path}/expression_data_{database}_{repertoire}_AIRR.tsv",
        index=False,
        sep="\t",
    )
    # export flow data/expression data

    # Open/create the HDF5 file #todo save as dataframe not hdf5
    # sa, saType = sr.df_to_sarray(flow_df.reset_index())
    # f = h5py.File(f"{base_path}/expression_data_{database}.hdf5", "w")
    # # Save the structured array
    # f.create_dataset("flowdata", data=sa, dtype=saType)
    # f.close()


if __name__ == "__main__":
    logger.info("[START] AIRR export started")
    try:
        main()
        logger.info("[SUCCESS] Database {} has been exported AIRR")
        logger.info("[END]")
    except BaseException as e:
        logger.error(f"ups, something went wrong {e}")
