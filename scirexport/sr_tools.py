"""
Set of functions used by airr_export.py
for exporting a database in an AIRR v 1.3-4 compliant format

"""
import pandas as pd
import airr
from collections import OrderedDict
import re, ast, json, yaml, os, itertools, copy
from configobj import ConfigObj
import MySQLdb.cursors
from sshtunnel import SSHTunnelForwarder
from os.path import expanduser
import numpy as np
from typing import Union
import yamlordereddictloader
from scirexport.logger import logger

# relative package paths
this_dir, _ = os.path.split(__file__)

# the config file for mariadb databases
config = ConfigObj(expanduser("~/.my.cnf"))
# the AIRR schema v 1.3
schema_file_path = os.path.join(this_dir, "templates", "airr-schema.yaml")
# the igdb_project.sql script
mysql_file = os.path.join(this_dir, "templates", "igdb_project.sql")
# load schema file
with open(schema_file_path, "r") as schema_file:
    airr_schema = yaml.load(schema_file, Loader=yamlordereddictloader.Loader)

with open(
    os.path.join(this_dir, "templates", "airr_scireptor.json"), "r"
) as airr_extended_dic_file:
    airr_scireptor_json = json.load(airr_extended_dic_file)

#  the template fastaq.info file with the D130 standard metadata annotations
meta_template_path = os.path.join(this_dir, "templates", "template.fasta.fastq.info")


def parse_meta_template(path_meta_template=meta_template_path):
    with open(path_meta_template, "r") as fasta_info:
        meta_template = {}
        for line in fasta_info:
            line = line.partition("#")[0].rstrip().split("=")
            if line[0] and line[1]:
                meta_template[line[0]] = line[1]
        return (
            pd.DataFrame.from_dict(meta_template, orient="index")
            .T.replace({"True": True, "False": False})
            .to_dict("records")[0]
        )


# main functions
def connect(conf_db: Union[dict, str], database: str = "", ssh_group: str = None):
    """
    connect to db

    :param conf_db: the configuration name of the database in the config file
    :param database: database name
    :param ssh_group: use ssh tunneling
    :return: MySQLdb.connect object
    """

    if type(conf_db) == dict:
        if database:
            conf_db["database"] = database

    if database and type(conf_db) != dict:
        config[conf_db]["database"] = database

    if not ssh_group:
        return MySQLdb.connect(**config[conf_db])

    elif ssh_group and "test" in ssh_group:
        return MySQLdb.connect(
            host=conf_db["host"],
            user=conf_db["user"],
            password=conf_db["password"],
            port=conf_db["port"],
            database=database,
        )

    elif ssh_group and "test" not in ssh_group:
        server = SSHTunnelForwarder(
            (config[ssh_group]["address"], int(config["ssh_sr"]["port"])),
            ssh_username=config[ssh_group]["user"],
            ssh_private_key=config[ssh_group]["privatekey"],
            ssh_private_key_password=config[ssh_group]["password"],
            remote_bind_address=("127.0.0.1", 3306),
        )

        server.start()
        config[conf_db]["host"] = "127.0.0.1"
        config[conf_db]["port"] = server.local_bind_port

        db = MySQLdb.connect(**config[conf_db])

        db = db[1] if type(db) in [list, tuple] else db

        return server, db


def get_config(config_file="config"):

    """
    Looks for config file in ./

    :param config_file:
    :return: ConfigObj dictionary.
    """

    # try to open config file in .
    try:
        config_file = open(config_file, "r")
    except IOError:
        raise Exception("no config file found")
    return ConfigObj(config_file)


if __name__ == "__main__":
    connect()
    get_config()


def parse_schema(spec, schema):

    """
    Parse an AIRR schema object for doc tables
    adapted from @airr-community

    :param spec: name of the schema object
    :param schema: master schema dictionary parsed from the yaml file.
    :return: list of dictionaries with parsed rows of the spec table.
    """

    data_type_map = {
        "string": "free text",
        "integer": "positive integer",
        "number": "positive number",
        "boolean": "true | false",
    }

    # Get schema
    properties = schema[spec]["properties"]
    required = schema[spec].get("required", None)

    # Iterate over properties
    table_rows = []
    for prop, attr in properties.items():
        # Standard attributes
        required_field = False if required is None or prop not in required else True
        title = attr.get("title", "")
        example = attr.get("example", "")
        description = attr.get("description", "")

        # Data type
        data_type = attr.get("type", "")
        data_format = data_type_map.get(data_type, "")

        # Arrays
        if data_type == "array":
            if attr["items"].get("$ref") is not None:
                sn = attr["items"].get("$ref").split("/")[-1]
                data_type = "array of :ref:`%s <%sFields>`" % (sn, sn)
            elif attr["items"].get("type") is not None:
                data_type = "array of %s" % attr["items"]["type"]
        elif attr.get("$ref") == "#/Ontology":
            data_type = ":ref:`Ontology <OntoVoc>`"
        elif attr.get("$ref") is not None:
            sn = attr.get("$ref").split("/")[-1]
            data_type = ":ref:`%s <%sFields>`" % (sn, sn)

        # x-airr attributes
        if "x-airr" in attr:
            xairr = attr["x-airr"]
            nullable = xairr.get("nullable", True)
            deprecated = xairr.get("deprecated", False)
            identifier = xairr.get("identifier", False)

            # MiAIRR attributes
            miairr_level = xairr.get("miairr", "")
            miairr_set = xairr.get("set", "")
            miairr_subset = xairr.get("subset", "")

            # Set data format for ontologies and controlled vocabularies
            if "format" in xairr:
                if xairr["format"] == "ontology" and "ontology" in xairr:
                    base_dic = xairr["ontology"]
                    ontology_format = (
                        str(base_dic["top_node"]["id"]),
                        str(base_dic["top_node"]["label"]),
                    )
                    # Replace name with url-linked name
                    data_format = (
                        "{'Ontology': {'top_node': {'id': '%s', 'value': '%s'}}}"
                        % ontology_format
                    )
                    # Get 'type' for ontology
                    example = "id: %s, value: %s" % (example["id"], example["label"])
                elif xairr["format"] == "controlled vocabulary":
                    if attr.get("enum", None) is not None:
                        data_format = "Controlled vocabulary: %s" % ", ".join(
                            attr["enum"]
                        )
                    elif attr.get("items", None) is not None:
                        data_format = "Controlled vocabulary: %s" % ", ".join(
                            attr["items"]["enum"]
                        )
        else:
            nullable = True
            deprecated = False
            identifier = False
            miairr_level = ""
            miairr_set = ""
            miairr_subset = ""

        if deprecated:
            field_attributes = "DEPRECATED"
        else:
            f = [
                "required" if required_field else "optional",
                "identifier" if identifier else "",
                "nullable" if nullable else "",
            ]
            field_attributes = ", ".join(filter(lambda x: x != "", f))

        # Return dictionary
        r = {
            "Name": prop,
            "Set": miairr_set,
            "Subset": miairr_subset,
            "Designation": title,
            "Field": prop,
            "Type": data_type,
            "Format": data_format,
            "Definition": description,
            "Example": example,
            "Level": miairr_level,
            "Required": required_field,
            "Deprecated": deprecated,
            "Nullable": nullable,
            "Identifier": identifier,
            "Attributes": field_attributes,
        }

        table_rows.append(r)

    return table_rows


def schema_dicts():

    """
    creates a dictionary from  the AIRR schema v 1.3 for validating airr formats
    :return: dictionary for validating airr formats
    """
    # Iterate over schema and build reference tables
    data_elements = OrderedDict()

    for spec in airr_schema:
        if "properties" not in airr_schema[spec]:
            continue
        else:
            data_elements[spec] = parse_schema(spec, airr_schema)

    objects_to_rep = [
        "Repertoire",
        "Study",
        "Subject",
        "Diagnosis",
        "Sample",
        "CellProcessing",
        "NucleicAcidProcessing",
        "PCRTarget",
        "SequencingRun",
        "RawSequenceData",
        "DataProcessing",
    ]

    airr_objects = objects_to_rep + ["Rearrangement", "Cell"]
    # create validation dict

    validate_airr_dict = OrderedDict()

    for spec in airr_objects:
        for r in data_elements[spec]:
            if not r["Deprecated"]:
                validate_airr_dict[r["Name"]] = r
                validate_airr_dict[r["Name"]].update({"Object": spec})
                if spec in objects_to_rep:
                    validate_airr_dict[r["Name"]].update({"File": "Repertoire"})
                elif r["Name"] == "repertoire_id":
                    validate_airr_dict[r["Name"]].update({"Object": "Repertoire"})
                    validate_airr_dict[r["Name"]].update(
                        {"File": "Repertoire/Rearrangement"}
                    )
                else:
                    validate_airr_dict[r["Name"]].update({"File": "Rearrangement"})

    return validate_airr_dict


# get dictionary from AIRR schema needed for processing
validate_airr_dic = schema_dicts()
# create a dictionary to check formats of the properties to map


def unwrap_add_info(df: pd.DataFrame) -> pd.DataFrame:
    """
    unwraper for encoded `add_{}_info` columns of sciReptor

    :param df: dataframe containing the `add_{}_info` columns
    :return: dataframe with the expanded `add_{}_info` columns
    """

    col_list = [
        "add_sample_info",
        "add_pcr_info",
        "add_donor_info",
        "add_sequencing_info",
        "background_treatment",
    ]

    append_cols = []
    col_list_filter = [
        x
        for x in col_list
        if x in df.columns
        and (len(df[x].unique().tolist()) > 1 or "" not in df[x].unique().tolist())
    ]

    for column in col_list_filter:

        newcols = df[column].str.split(",", expand=True)
        append_single_cols = []

        for i in newcols.columns:

            nested_df = newcols.iloc[:, i].str.split("=", expand=True)
            # ignore columns that don't have a value
            if len(nested_df.columns) < 2:
                continue
            unstacked_df = pd.pivot(nested_df, columns=0, values=1)
            if np.NaN in unstacked_df.columns:
                unstacked_df = pd.pivot(nested_df, columns=0, values=1).drop(
                    columns=[np.NaN]
                )
            append_single_cols.append(unstacked_df)
        if len(append_single_cols) < 1:
            logger.warning(
                f"Column {column} is empty in the DB, some fields won`t be mapped"
            )
            continue
        appended_cols = pd.concat(append_single_cols, axis=1, join="inner")

        # string join to keep repeated column values
        def sjoin(x):

            return ";".join(x[x.notnull()].astype(str))

        appended_cols = appended_cols.groupby(level=0, axis=1).apply(
            lambda x: x.apply(sjoin, axis=1)
        )

        append_cols.append(appended_cols)

        if column == "add_donor_info":  # split age column in _min, _max, _unit

            if "age" in appended_cols.columns:
                result_df = (
                    appended_cols["age"]
                    .str.split(r"(\d+)([A-Za-z]+)", expand=True)
                    .loc[:, [1, 2]]
                )
                result_df.rename(columns={1: "age_min", 2: "age_unit"}, inplace=True)
                result_df["age_max"] = result_df["age_min"]

        elif column == "add_sample_info":
            if "time_point" in appended_cols.columns:
                # split time_point in collection_time_point_relative , _unit
                result_df = (
                    appended_cols["time_point"]
                    .str.split(r"(\d+)([A-Za-z]+)", expand=True)
                    .loc[:, [1, 2]]
                ).rename(
                    columns={
                        1: "collection_time_point_relative",
                        2: "collection_time_point_relative_unit",
                    },
                )
            elif "time_point_ref" in appended_cols.columns:
                result_df = appended_cols.rename(
                    columns={
                        "time_point_ref": "collection_time_point_reference",
                    },
                )
            # else:
            #     result = appended_cols
        else:
            result_df = appended_cols

        # remove duplicate columns before appending dgs
        df = df.drop(columns=[x for x in result_df.columns if x in df.columns])
        append_cols.append(result_df)
    append_cols.append(df)
    appended_cols_all = pd.concat(append_cols, axis=1, copy=False)
    return appended_cols_all


def airrize_df(
    unwrapped_df: pd.DataFrame, receptor_type: str, dict_validate: dict
) -> pd.DataFrame:
    """
    change column names of the scireptor dataframe mapped to their AIRR equivalent
    :param unwrapped_df: scireptor dataframe with expanded 'add_.*_info' columns
    :param receptor_type: type of receptor inferred from the studies table name
    :param dict_validate: dictionary from AIRR schema parsed with schema_dicts()
    :return: dataframe with proper AIRR column names

    """
    types_dic = {
        "string": "str",
        "integer": "float",
        "number": "float",
        ":ref:`Ontology <OntoVoc>`": "str",
        "boolean": "bool",
        "array of :ref:`Diagnosis <DiagnosisFields>`": "str",
        ":ref:`RawSequenceData <RawSequenceDataFields>`": "str",
    }
    rename_dic = {}
    missing_to_map = []
    data_to_airrize = unwrapped_df
    format_dict = {}

    for airr_name in airr_scireptor_json.keys():

        if "column" not in airr_scireptor_json[airr_name]:
            continue  # ignore non assigned fields (should all be non-miairr)

        elif airr_scireptor_json[airr_name]["column"] == "":
            logger.warning(f"Property {airr_name} has not yet a place in the DB")
            continue

        main_col = airr_scireptor_json[airr_name]["column"]
        if [x for x in data_to_airrize.columns.tolist() if main_col in x]:
            format_dict[airr_name] = types_dic[validate_airr_dic[airr_name]["Type"]]

            if "expanded_column" not in airr_scireptor_json[airr_name]:
                old_col = main_col

            else:
                if type(airr_scireptor_json[airr_name]["expanded_column"]) == dict:

                    if main_col == "name":
                        old_col = f"{main_col}_{airr_scireptor_json[airr_name]['expanded_column']['type']}"

                    elif main_col in ["start", "end", "dna_seq", "prot_seq"]:
                        old_col = f"{main_col}_{airr_scireptor_json[airr_name]['expanded_column']['region']}"

                    else:
                        old_col = main_col
                else:

                    old_col = airr_scireptor_json[airr_name]["expanded_column"]

        else:
            continue

        rename_dic[old_col] = airr_name
        missing_to_map.append(airr_name)

    airrized = data_to_airrize.rename(columns=rename_dic, inplace=False, copy=False)
    airrized = airrized[
        [x for x in airrized.columns if x in dict_validate.keys()]
    ].dropna(axis=1, how="all")

    if (
        "locus" in airrized.columns
        and False in airrized["locus"].str.match(receptor_type).tolist()
    ):
        airrized["locus"] = receptor_type + airrized["locus"]

    format_dict = {
        x: format_dict[x] for x in format_dict if x in airrized.columns.tolist()
    }

    # empty_cols = [col for col in airrized.columns if airrized[col].isnull().all()]
    # Drop these columns from the dataframe
    # airrized.drop(empty_cols, axis=1, inplace=True)

    try:
        return airrized.loc[:, ~airrized.columns.duplicated()].astype(format_dict)
    except ValueError:
        print(
            airrized[
                [
                    x
                    for x in format_dict
                    if format_dict[x] == "float" and x in airrized.columns
                ]
            ].head()
        )


# return mysql file as string for pymysql
def mysql_as_string(mysql_file):
    with open(mysql_file, "r") as mysql_f:
        mysql_string = mysql_f.read()
    return mysql_string


def validate_columns(airrized_df, replace_ontos):
    """
    :param airrized_df: a df with AIRR column names
    :param replace_ontos: dictionary made from the database
    :return: a df with the proper formats and types defined by the airr-community
    """

    replace_ontos = {x: replace_ontos[x]["ontology_id"] for x in replace_ontos}

    replace_ontos[np.NaN] = None

    cols_replacements = {
        "productive": {0: "F", 1: "T", np.NaN: None},
        "stop_codon": {0: "F", 1: "T"},
        "cell_subset": replace_ontos,
        "species": replace_ontos,
        "age_unit": replace_ontos,
        "tissue": replace_ontos,
        "cell_species": replace_ontos,
        "collection_time_point_relative_unit": replace_ontos,
    }

    airrized_df = airrized_df.replace(cols_replacements)

    for k in validate_airr_dic.keys():

        format_prop = validate_airr_dic[k]["Format"]
        if format_prop not in ["free text"] and k in airrized_df.columns.tolist():

            if format_prop in ["positive number", "positive integer"]:
                if [x for x in airrized_df[k].tolist() if x < 0]:
                    raise TypeError(f"there should not be any negative number in {k}")

            elif "Ontology" in format_prop:
                onto_dic = ast.literal_eval(format_prop)
                ontology = onto_dic["Ontology"]["top_node"]["id"].split(":")[0]

                # check that ontologies are properly formatted
                re_string = fr"{ontology}:.\d{{1,10}}"
                check_set = set(x for x in airrized_df[k] if x)
                if not check_set or airrized_df[k].empty:
                    continue
                elif not [x for x in check_set if re.match(re_string, x)]:
                    for element in check_set:
                        while True:
                            replace_ontos[element] = input(
                                f"""> {element} is not represented as a CURIE of {ontology}
                                    please provide a valid ontology for {element}: """
                            )
                            if not re.match(re_string, replace_ontos[element]):
                                logger.error(
                                    f"""{replace_ontos[element]} must be CURIE on the format:
                                        '{onto_dic['Ontology']['top_node']['id']}'"""
                                )
                            else:
                                logger.info(f"CURIE {replace_ontos[element]} updated")
                                break

            else:
                if "Controlled vocabulary" in format_prop:  # this can be simplified

                    controlled_voc = [
                        x.strip()
                        for x in format_prop.split("vocabulary:")[1].split(",")
                    ]
                    list_of_vocs = airrized_df[k].unique().tolist()[0]
                    if list_of_vocs is None:
                        continue
                    elif set(list_of_vocs.split(",")).issubset(controlled_voc):
                        continue
                    elif [[z.strip() for z in x.split(",")] for x in list_of_vocs]:
                        continue
                    else:
                        logger.error(
                            f"{list_of_vocs} not defined as any of: {controlled_voc}"
                        )
                        raise TypeError(
                            f"{list_of_vocs} not defined as any of: {controlled_voc}"
                            f"{list_of_vocs} not defined as any of: {controlled_voc}"
                        )

                elif "true | false" in format_prop:
                    if [
                        x
                        for x in airrized_df[k].unique().tolist()
                        if x not in ["T", "F", True, False, None]
                    ]:
                        raise TypeError(
                            f'{k} must be in format ["T","F", True, False], current values are: {airrized_df[k].unique().tolist()}'
                        )
                else:
                    logger.warning(f"{k, format_prop} did not go through validation")

    airrized_df = airrized_df.replace(cols_replacements)
    return airrized_df


def query_receptor(cursor, val_tuple, mode=""):

    """
    function to query for existing receptors for a Heavy and Light chain pair

    :param cursor: cursor of the current db connection
    :param col_tuple: tuple with the column names to query i.e (IGH,IGK)
    :param val_tuple: tuple of the aminoacid H/L chains to query
    :param mode: test or empty
    :return: query result for a match on found receptor_id
    """
    if "test" in mode:
        receptors_db = "test_receptors"
    else:
        receptors_db = "scireptor_receptors"

    # invert list of values to avoid duplicate by chain position
    inverted_val = val_tuple[2:] + val_tuple[:2]
    query_receptor_sq = f"""SELECT * FROM `{receptors_db}`.`receptors` where
    (`chain_1` = '%s' and `locus_1` = '%s' and `chain_2` = '%s' and `locus_2` = '%s') ;"""
    cursor.execute(query_receptor_sq % val_tuple)
    found = cursor.fetchone()
    if not found:
        cursor.execute(query_receptor_sq % inverted_val)
        found = cursor.fetchone()
        return found
    return found


def receptor_id_assign(airr_dataframe, receptor_type, db, mode=""):

    """
    generates valid paired receptors from the current database,
    checks for their existence in the scireptor_receptor database and
    if not been previously observed creates a new receptor_id entry for each in the
    scireptor_receptor database.

    :param airr_dataframe: AIRR formatted dataframe with productive IGH & IGK/L or TCA & TCB rearrangements
    :param receptor_type: type of receptor inferred from the studies table name
    :param db: database name
    :param mode: test or empty
    :return: AIRR dataframe with assigned or found receptor_id and a receptor_id dictionary to be exported in json format
    """

    if "test" in mode:
        receptors_db = "test_receptors"
    else:
        receptors_db = "scireptor_receptors"

    c = db.cursor()
    c.execute(
        f"SELECT receptor_id FROM `{receptors_db}`.`receptors` ORDER BY receptor_id DESC LIMIT 1;"
    )
    last_id = c.fetchone()  # get the last inserted id on the db

    if receptor_type == "IG":
        loci_1 = ["IGH"]
        loci_2 = ["IGK", "IGL"]
    elif receptor_type == "TCR":
        loci_1 = ["TCRA"]
        loci_2 = ["TCRB"]

    if not last_id:
        last_id = [0]

    receptor_id_count = itertools.count(int(last_id[0]) + 1)
    # ^counter for ids starting on the last inserted on the db
    logger.info(f"last registered receptor_id={int(last_id[0])}")
    receptor_id_dic = {}  # dictionary for ids
    receptor_dfs = []
    airr_dataframe["receptor_id"] = np.nan
    fix_receptor_id = int(last_id[0])
    sql_insert = f"INSERT INTO `{receptors_db}`.`receptors` (`receptor_id`, `chain_1`, `locus_1`, `chain_2`, `locus_2`) VALUES (%s,%s,%s,%s,%s);"

    # find cells with more than two rearrangements
    for cell in airr_dataframe.cell_id.unique().tolist():
        if airr_dataframe.cell_id.tolist().count(cell) >= 2:
            cell_df = airr_dataframe[airr_dataframe.cell_id == cell]
            # assign one `receptor_id` if a cell has two valid rearrangements,, one H and one K/L
            if any(i in loci_1 for i in cell_df.locus.tolist()) and any(
                i in loci_2 for i in cell_df.locus.tolist()
            ):

                cell_df = cell_df.drop_duplicates(
                    subset=["sequence_aa", "locus"], keep="first", inplace=False
                )

                cell_loc_dic = (
                    cell_df[["sequence_aa", "sequence_id", "cell_id", "locus"]]
                    .set_index("sequence_aa")
                    .to_dict("index")
                )

                if len(cell_df) == 2:
                    # ^if a cell has two valid chains
                    loci_tuple = [
                        (seq, cell_loc_dic[seq]["locus"]) for seq in cell_loc_dic
                    ]

                    ids_tuple = tuple(
                        [cell_loc_dic[seq]["sequence_id"] for seq in cell_loc_dic]
                    )
                    query_list = loci_tuple[0] + loci_tuple[1]
                    found_receptor = query_receptor(c, query_list, mode)

                    if found_receptor:
                        # if receptor has already been observed add to the dict and continue
                        receptor_id_dic[found_receptor[0]] = {
                            "seq_loci": {
                                x: cell_loc_dic[x]["locus"] for x in cell_loc_dic
                            },
                            "ids_seq": ids_tuple,
                        }
                        receptor_dfs.append(cell_df)

                    else:
                        # if receptor hasn't been observed before, insert in db
                        fix_receptor_id = next(receptor_id_count)
                        c.execute(sql_insert, ((fix_receptor_id,) + query_list))
                        receptor_id_dic[fix_receptor_id] = {
                            "seq_loci": {
                                x: cell_loc_dic[x]["locus"] for x in cell_loc_dic
                            },
                            "ids_seq": ids_tuple,
                        }

                        receptor_dfs.append(cell_df)

                # assign list of `receptor_id`s if a cell has more than two valid rearrangements
                elif len(cell_df) >= 3:
                    list_1 = cell_df[cell_df.locus.isin(loci_1)].sequence_aa.tolist()
                    list_2 = cell_df[cell_df.locus.isin(loci_2)].sequence_aa.tolist()
                    # create combined list of chain_1  with chain_2 sequence`s
                    combos = itertools.product(list_1, list_2)

                    for x, y in combos:

                        query_tuple = (
                            x,
                            cell_loc_dic[x]["locus"],
                            y,
                            cell_loc_dic[y]["locus"],
                        )
                        ids_tuple = (
                            cell_loc_dic[x]["sequence_id"],
                            cell_loc_dic[y]["sequence_id"],
                        )
                        found_receptor = query_receptor(c, query_tuple, mode)

                        if found_receptor:
                            # if receptor has already been observed add to the dict and continue
                            receptor_id_dic[int(found_receptor[0])] = {
                                "seq_loci": {
                                    x: cell_loc_dic[x]["locus"] for x in (x, y)
                                },
                                "ids_seq": ids_tuple,
                            }
                        else:

                            fix_receptor_id = next(receptor_id_count)

                            values_insert = (
                                fix_receptor_id,
                                x,
                                cell_loc_dic[x]["locus"],
                                y,
                                cell_loc_dic[y]["locus"],
                            )
                            receptor_id_dic[fix_receptor_id] = {
                                "seq_loci": {
                                    x: cell_loc_dic[x]["locus"] for x in (x, y)
                                },
                                "ids_seq": ids_tuple,
                            }
                            c.execute(sql_insert, values_insert)

                    receptor_dfs.append(cell_df)

            else:
                receptor_dfs.append(cell_df)  # keep cells with >2 not valid rearr

        else:
            not_cell_df = airr_dataframe[
                airr_dataframe.cell_id == cell
            ]  # keep cells with only one rearrangement
            receptor_dfs.append(not_cell_df)

    miairr_redecptordf = pd.concat(receptor_dfs, join="inner", axis=0)

    d = {}
    for receptor_id, value in sorted(receptor_id_dic.items()):
        for val in value["ids_seq"]:
            d.setdefault(val, list()).append((str(receptor_id)))

    miairr_redecptordf["receptor_id"] = miairr_redecptordf["sequence_id"].map(
        {x: ",".join(v) for x, v in d.items()}
    )
    receptor_id_dic = {x: v["seq_loci"] for x, v in receptor_id_dic.items()}
    db.commit()

    logger.info(f"last inserted receptor_id={str(fix_receptor_id)}")
    return miairr_redecptordf, receptor_id_dic


def dump_repertoire(meta_complete_dict: dict):

    """
    generates a valid YAML repertoire object for the current parsed repertoire

    :param meta_complete_dict: AIRR formatted dataframe with productive IGH & IGK/L rearrangements
    :return: a valid yaml object for a repertoire
    """

    new_temp = airr.repertoire_template()

    ziped_runs = zip(
        meta_complete_dict["sequencing_run_id"].split(","),
        meta_complete_dict["sequencing_run_date"].split(","),
    )
    meta_complete_dict["keywords_study"] = meta_complete_dict["keywords_study"].split(
        ","
    )

    for x in new_temp:
        if x in meta_complete_dict:
            new_temp[x] = str(meta_complete_dict[x])
        elif x not in ["sample", "data_processing"]:
            for z in new_temp[x]:
                if z in meta_complete_dict:
                    new_temp[x][z] = meta_complete_dict[z]

        elif x == "sample":
            for t in new_temp[x][0].keys():

                if t == "pcr_target":
                    loci = []
                    for locus in meta_complete_dict["pcr_target_locus"].split(","):
                        new_temps = copy.deepcopy(new_temp[x][0][t][0])
                        new_temps["pcr_target_locus"] = locus
                        # todo clean this exception for *pcr_primer_target_location
                        if any(
                            i in meta_complete_dict
                            for i in [
                                "forward_pcr_primer_target_location",
                                "reverse_pcr_primer_target_location",
                            ]
                        ):
                            new_temps[
                                "forward_pcr_primer_target_location"
                            ] = meta_complete_dict["forward_pcr_primer_target_location"]
                            new_temps[
                                "reverse_pcr_primer_target_location"
                            ] = meta_complete_dict["reverse_pcr_primer_target_location"]
                        loci.append(new_temps)
                        new_temp[x][0][t] = loci

                elif t in meta_complete_dict:
                    if (
                        t == "collection_time_point_relative"
                    ):  # add until v 1.4 includes time point unit
                        new_temp[x][0][
                            t
                        ] = f"{meta_complete_dict['collection_time_point_relative']}"
                    else:
                        new_temp[x][0][t] = meta_complete_dict[t]

        elif x == "data_processing":
            for t in new_temp[x][0].keys():
                if (
                    t == "data_processing_files"
                ):  # add until v 1.4 includes time point unit
                    new_temp[x][0][t] = [meta_complete_dict["data_processing_files"]]

        else:
            for t in new_temp[x][0].keys():
                if t in meta_complete_dict:
                    new_temp[x][0][t] = meta_complete_dict[t]

    samples = []
    for run_id, run_date in ziped_runs:
        sample_copy = copy.deepcopy(new_temp["sample"][0])
        sample_copy["sequencing_run_id"] = str(run_id)
        sample_copy["sequencing_run_date"] = run_date
        samples.append(sample_copy)
    new_temp["sample"] = samples

    return new_temp


def df_to_sarray(df: pd.DataFrame) -> (np.array, np.dtype):
    """
    Convert a pandas DataFrame object to a numpy structured array.
    Also, for every column of a str type, convert it into
    a 'bytes' str literal of length = max(len(col)).

    :param df: the data frame to convert
    :return: a numpy structured array representation of df
    """

    def make_col_type(col_type, col):

        if "numpy.object_" in str(col_type.type):
            maxlens = col.dropna().str.len()
            if maxlens.any():
                maxlen = maxlens.max().astype(int)
                col_type = ("S%s" % maxlen, 1)
            else:
                col_type = "f2"
        return col.name, col_type

    v = df.values
    types = df.dtypes
    numpy_struct_types = [
        make_col_type(types[col], df.loc[:, col]) for col in df.columns
    ]
    dtype = np.dtype(numpy_struct_types)
    z = np.zeros(v.shape[0], dtype)
    for (i, k) in enumerate(z.dtype.names):

        if dtype[i].str.startswith("|S"):
            z[k] = df[k].str.encode("latin").astype("S")
        else:
            z[k] = v[:, i]

    return z, dtype
