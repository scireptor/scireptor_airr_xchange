#!/usr/bin/env python3

"""
Author
git@franasa - f.arcila@dkfz-heidelberg.de

Description
Main script of the scirexport importer,
imports all MiAIRR single-cell sequencing data
from the five AIRR 1.3-4 exchange files
https://github.com/airr-community/airr-standards/issues/409

Input directory must contain the following files:

- rearrangements_{project_name}_AIRR.tsv
- repertoires_{project_name}_AIRR.yaml
- expression_data_{project_name}.h5
- cells_{project_name}_AIRR.json
- receptors_{project_name}_AIRR.json
"""


import os
import pandas as pd
import json
import argparse
import airr
from argparse import RawTextHelpFormatter

# custom imports
from scirexport.logger import logger
import scirexport.sr_tools as sr
from scirexport.importools import (
    import_FACS_expression,
    import_10X_expression,
    import_repertoires,
    import_rearrangements,
    import_receptors,
    safe_create_schema,
)

# define args
parser = argparse.ArgumentParser(
    description=__doc__, formatter_class=RawTextHelpFormatter
)
parser.add_argument(
    "-n", "--project_name", help="name of the scireptor database to importer"
)
parser.add_argument(
    "-g",
    "--mariadb_group",
    help="name of the MariaDB group on the `.my.cnf` config file with connection parameters",
)
parser.add_argument("-i", "--input_path", help="path to the folder with the AIRR files")
parser.add_argument(
    "-s",
    "--ssh_group",
    help="name of the MariaDB group on the `.my.cnf` config file with connection parameters",
)

# get variables
args = parser.parse_args()

DB_ARGS = {
    "project_name": args.project_name,
    "config_db": args.mariadb_group,
    "ssh_group": args.ssh_group,
    "input_path": args.input_path,
}


def main():

    """
    main function of the importer
    :return:
    """

    config_db = DB_ARGS["config_db"]
    project_name = DB_ARGS["project_name"]
    files_path = DB_ARGS["input_path"]
    mode = ""

    # make a dictionary with file_names:paths
    files_dictionary = {
        file.split("_")[0]: os.path.join(files_path, file)
        for file in os.listdir(files_path)
    }
    # get map to json file
    map_json = sr.airr_scireptor_json
    mysql_file = sr.mysql_file

    if DB_ARGS["ssh_group"] and "test" not in DB_ARGS["ssh_group"]:
        try:
            # first top-level connection for shh tunneling
            server, db = sr.connect(conf_db=config_db, ssh_group=DB_ARGS["ssh_group"])
            safe_create_schema(db, project_name)
            server.stop()

            # main connection
            server, db = sr.connect(
                conf_db=config_db,
                database=project_name,
                ssh_group=DB_ARGS["ssh_group"],
            )

        except Exception as error:
            logger.error(f"MariaDB ERROR {error}")
            raise (error)

    elif DB_ARGS["ssh_group"] and "test" in DB_ARGS["ssh_group"]:
        mode = "test"
        db = sr.connect(conf_db=config_db, ssh_group=DB_ARGS["ssh_group"])
        safe_create_schema(db, project_name)
        # main connection
        db = sr.connect(
            conf_db=config_db, database=project_name, ssh_group=DB_ARGS["ssh_group"]
        )

    else:
        # first toplevel connection
        db = sr.connect(conf_db=config_db)

        safe_create_schema(db, project_name)
        # main connection
        db = sr.connect(conf_db=config_db, database=project_name)

    # Create database tables from templates/igdb_project.sql
    c = db.cursor()
    c.execute(sr.mysql_as_string(mysql_file))

    # @@@@ get files @@@@@ #
    # 1  rearrangements DF
    rearrangements = pd.read_csv(files_dictionary["rearrangements"], sep="\t")

    # 2 get receptors, normalize dictionary
    if "receptors" in files_dictionary:
        with open(files_dictionary["receptors"], "r") as json_file:
            receptors_json = json.load(json_file)
        import_receptors(receptors_json, db, mode=mode)
    else:
        logger.warning("No receptors file was provided. No receptors will be imported")

        # 5 expression DF/s
    if "expression" in files_dictionary:
        if "facs" in files_dictionary["expression"].lower():
            # expression_hdf = pd.read_csv(files_dictionary["expression"], sep="\t")
            import_FACS_expression(files_dictionary["expression"], project_name, db)
        elif "10x" in files_dictionary["expression"].lower():
            import_10X_expression(files_dictionary["expression"], project_name, db)
    else:
        logger.warning(
            "No expression file was provided. No expression data will be imported"
        )

    # 3 cells DF
    # if 'cells' in files_dictionary:
    #     cells = pd.read_json(files_dictionary['cells'], orient='index')
    # else:
    #     logger.warning(
    #         "cells file was not provided no cells will be imported"
    #     )

    # 4 get repertoires and validate them with AIRR

    repertoires_json = airr.load_repertoire(
        files_dictionary["repertoires"], validate=True, debug=True
    )

    # Error inception dict (repertores:sample:is_single_cell)
    # if any repertoire has 'single_cell' != 'T' abort import

    if {
        repertoire["repertoire_id"]: {
            x["sample_id"]: x["single_cell"] for x in repertoire["sample"]
        }
        for repertoire in repertoires_json["Repertoire"]
        if {
            x["sample_id"]: x["single_cell"]
            for x in repertoire["sample"]
            if not x["single_cell"]
        }
    }:
        raise TypeError("Repertoires don't contain any single_cell=true samples")
    else:
        pass

    import_repertoires(repertoires_json, map_json, project_name, db)

    import_rearrangements(rearrangements, map_json, project_name, db)

    db.close()
    if DB_ARGS["ssh_group"] and "test" not in DB_ARGS["ssh_group"]:
        server.stop()

    return True


if __name__ == "__main__":
    logger.debug("Import started")
    try:
        main()
        logger.info("[SUCCESS] AIRR import into scireptorDB has finished")
        logger.info("program ended")
    except BaseException:
        logger.error("ups, something went wrong", exec_info=True)
