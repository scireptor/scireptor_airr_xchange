from setuptools import setup

setup(
    name="scirexport",
    version="0.2",
    description="AIRR exporter for scireptor",
    url="https://gitlab.com/b-cell-immunology/scireptor_airr_exporter",
    author="Francisco Arcila",
    author_email="f.arcila@dkfz-heidelberg.de",
    license="GNU Affero General Public License v3.0",
    packages=["scirexport"],
    package_data={"scirexport": ["templates/*"]},
    keywords=[
        "bioinformatics",
        "immunology",
        "AIRR",
        "Rep-Seq",
        "single cell sequencing",
        "IG",
        "AIRR-seq",
        "B cell",
    ],
    scripts=["scirexport/airr_export.py"],
    entry_points={
        "console_scripts": [
            "airr_export=scirexport.airr_export:main",
            "airr_import=scirexport.airr_import:main",
        ]
    },
    install_requires=[
        "h5py",
        "airr",
        "pandas",
        "numpy",
        "xlrd",
        "configobj",
        "pymysql",
        "yamlordereddictloader",
        "mysqlclient",
        "tables",
        "rich",
        "sshtunnel",
        "uuid",
    ],
    zip_safe=False,
)
